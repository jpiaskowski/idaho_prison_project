---
title: "What's Wrong with Idaho's Anti-trafficking Law?"
date: 2022-03-26
tags: ["sex trafficking", "sex work"]
description: While Idaho's 'Anti-Trafficking' law sounds like it is helping victims, it is criminalizing sex workers and their customers. 
thumbnail: /images/anti-human-trafficking-march.jpg
draft: true
---

*Photo: [Townsquare Media](https://www.townsquaremedia.com)*

Human trafficking in all the news these days and seems scary as hell -- an expression of utter inhumanity and depraved cruelty. Human trafficking, the forced servitude of individuals, is described by the U.S. Department of Justice as “a crime that involves exploiting a person for labor, services, or commercial sex.” 

Frequently, human trafficking shortened to the term “trafficking” and even more frequently, it’s discussed only in terms of sex trafficking. This has been a great concern to Americans! It sounds terribly frightening. Women, children, and other forcibly held and sexually exploited by pimps.

This has swept by celebrities from Carrie Underwood to support the cause of ‘ending human trafficking’, dozens upon dozens organizations devoted to this issue, loads of human interest stories in the media, any many many organizations devoted to this cause. Salesforce: https://www.salesforce.org/blog/7-nonprofits-leading-fight-human-trafficking/

February 1st is national sex trafficking day

There is organization after organization focused on this

Idaho KBOI trafficking series: https://www.kboi.com/2018/02/09/yes-human-trafficking-happens-here-in-idaho/

There is at least two organizations in Idaho devoted to stopping “sex trafficking”, the Idaho Anti-trafficking coalition (https://idahoatc.org/) and 

Even the International Academy of Trial Lawyers is fixated on sex trafficking over other forms of human trafficking. https://www.iatl.net/i4a/pages/index.cfm?pageid=3708

Polaris Project: https://polarisproject.org/
(part of Delta’s very limited number of recipient orgs for their charitable SkyWish program - https://www.delta.com/buygftxfer/displayDonateMiles.action)

Even featured in Forbes: https://www.forbes.com/sites/carmenniethammer/2020/02/02/cracking-the-150-billion-business-of-human-trafficking/
“Delta Air Lines’ SkyWish Program is an example of how a company leverages its resources in partnership with its customers and employees to help break cycles of abuse and ensure survivors have access to flight tickets and a way out.” 

### What is Human Trafficking?

The definition of human trafficking goes a bit beyond simply exploitation (who hasn't felt exploited in their job?). The International Labor Organization [defined](https://www.ilo.org/wcmsp5/groups/public/---ed_norm/---declaration/documents/publication/wcms_182004.pdf) “forced or compulsory labour” as “all work or service which is exacted from any person under the menace of any penalty and for which the said person has not offered himself voluntarily.” in 1930, a definition that still rings true. 

Human trafficking remains a [serious worldwide problem](https://www.ilo.org/global/about-the-ilo/newsroom/news/WCMS_182109/lang--en/index.htm), ensnaring an estimated 20.9 million people. 


### The Popular Conception of Human Trafficking 

The popular view of human trafficking is dominated by news and coverage of sex trafficking, particularly of children and young women. Just look [here](https://www.foxnews.com/us/missing-naomi-irion-family-group-abduction-sex-trafficking), [here](https://www.msn.com/en-us/health/wellness/supporting-survivors-of-sex-trafficking-a-call-for-compassionate-action-opinion/ar-AAVtXMJ), [here](https://www.dps.texas.gov/news/dps-arrests-35-joint-human-trafficking-operations) and [here.](https://www.ksl.com/article/50374602/21-arrested-in-utah-sex-trafficking-operation-police-say-it-is-not-even-the-tip-of-the-iceberg) 

A common trope is that the victim is a female, a minor and/or a prepubescent child. The victim is commonly represented as the [white girl](https://www.elle.com/culture/a36898189/0086-0088-megan-s-account-august-2021/) and/or child next door. The circumstances that lead to their trafficking is also often presented as a forceful kidnapping involving violence and/or drugging. 

**Renting Lacy**, a book by former U.S. Representative Linda Smith, whose cover features an adult and child shadows cast over a teddy bear lying on the ground of a playground, encapsulates this: 

> The average age of entry into prostitution in America is 13 years old. Forced into a life they never chose, manipulated, abused and tortured at the hands of the pimps who control them, our country's children are sold on the streets, on the internet and at truck stops across America every night. They aren't bad kids who made bad choices. They are victims of child sex trafficking. They come from our neighborhoods, our schools, our churches, and sometimes our own homes.
>   -- Linda Smith, **[Renting Lacy](https://www.amazon.com/Renting-Lacy-Americas-Prostituted-Children/dp/0976559463)**

### What Trafficking Really Looks like

For one, it's not just sex trafficking. In fact, a major source of human trafficking in the United States includes domestic workers and agricultural workers. 

Most of the victims are people are color and most are poor. 

US State Department identifies poverty as a major driver of human trafficking:
https://www.state.gov/reports/2021-trafficking-in-persons-report/
(very comprehensive report)

Victims of sex trafficking are not usually victims of dramatic kidnappings. Most often, they are pressured and coerced. 

Other estimates on trafficking: https://www.ilo.org/global/about-the-ilo/newsroom/news/WCMS_182109/lang--en/index.htm
Largely women, largely adults, largely forced non-sex labor (e.g. ag, construction, domestic)
https://www.ilo.org/global/about-the-ilo/newsroom/news/WCMS_182109/lang--en/index.htm
includes prison labor! 

### Major Anti-Trafficking Players

There are many many groups in the anti-sex trafficking landscape. Dozens if not hundreds exist in the United States, all with the aim of ending human trafficking. 

the players: 




ACLU on trafficking: https://www.aclu.org/other/human-trafficking-modern-enslavement-immigrant-women-united-states


1. The Anti-Sex Work Crusader
  These folks believe *all* sex work is exploitative and that no one willing would choose that career path. And no doubt, women, men and children have been forced into this. https://oceansrose.org/ Idaho anti-trafficking org, explicitly anti-prostitution
  
These organization sometimes even adopt the language of abolition, calling themselves “abolitionists”. https://sharedhope.org/about-us/our-mission-and-values/. These organizations have choosen to equate all form of sex work with trafficking and say outright that pimps and traffickers are the same. This group can be influenced by Christian focused organizations. Example: Shared Hope International (https://sharedhope.org). 

1. Christian-Focused Anti-Trafficking Organizations

1. The sex trafficking savior/action hero: these folks swoop in and literally save women and children. They may or may not offer help with care and recovery. Example: Destiny Rescue (this organization sounds pretty helpful, but one of their first stories concerns a minor who choose sex work out of desperation. Should we also focus on fixing the financial need that led to it?). https://www.destinyrescue.org/blog/words-from-hope-and-rosie/
overdramatic reporting on heroes rescuing children from trafficking: https://hopeforjustice.org/news-media/#video-modal

1. QANON. Hardly worth discussing because it is so misguided, but there are well meaning people who join a Qanon event under the aegis of “save the children”. I personally wish these folks would evaluate how these rallies and other events do anything to ‘save children’. [Operation Underground railroad](https://ourrescue.org/). 

Who is missing from this calculus? Victims. And sex workers.

### The Misinformation Universe

It's very hard to find reliable information on human and sex trafficking given the tremendous amount of misinformation flooding the interwebs on this. 

These organization can be well springs of bad statistics undergrid with Christian messaging. I doubt if the intention is to mislead, but if these organization lack a solid understanding of what trafficking truly looks like, how they possible alleviate the harms resulting from trafficking? 

Some examples: the average age of entry into prostitution is 13 and women who enter prostitution have an average life span of 7 years after beginning that work. Data does not support this premise (https://www.politifact.com/factchecks/2019/feb/27/mary-elizabeth-coleman/does-entering-prostitution-mean-you-have-seven-yea/). 

Shared Hope, a nonprofit run by former U.S. Representative Linda ….,  goes even further, couching their fears of child prostitution in terms of their general fear of youth sexuality. The guide to avoiding seeing your children swept up in sex trafficking includes serving on the textbook review committee and reviewing the sex ed curriculum at a child’s school. While some parents may want some control over sex ed curriculum, that is not at all related to child sex trafficking! There is no data suggesting that exposure to sex ed curriculum makes children vulnerable to sex trafficking. According to Shared Hope, the warning signs of vulnerable to sex trafficking include wanting to have sex, sadness when a boyfriend/girlfriend breaks up with them, and a desire to be loved. That sounds like rather normal teenage behavior (https://sharedhope.org/2022/01/24/guardrails/). 

The superbowl is estimated to bring in 20,000 trafficked victims, an astounding number given the superbowl typically draws an audience of 75,000 attendees (% men??)(source), a line repeated by  However, many sources have indicated this number to be false:

And the Global Alliance Against Traffic in Women indicates that not only is the predictions that large sporting events increases sex trafficking is incorrect, but lays out the conditions that associated with these events that are more likely to result in human labor traffcking: construction of new stadiums, productiion of athletic apparel and recruitment of athletes. (http://www.gaatw.org/publications/WhatstheCostofaRumour.11.15.2011.pdf). They also indicate that legalising sex work is a solution for reducing sex trafficking. “this idea continues to hold great appeal for prostitution abolitionist groups, anti-immigration groups, politicians and some journalists.” BINGO! This is a tool for anti-immigration and anti-sex work activists to use for their own ends, including fundraising and pushing policies that support their aims. 

article summarizing the misinformation: https://www.wtsp.com/article/news/verify/super-bowl-sex-trafficking-verify/67-5269c2f4-70e8-4eeb-9d45-24b0cc4bbf76

Also,
https://www.11alive.com/article/news/verify/crime-verify/no-evidence-super-bowl-is-biggest-sex-trafficking-event-in-the-world/536-85204501-2af8-4e7c-8376-8e5a45e1d672
and
https://www.cnn.com/2019/01/31/us/sex-trafficking-super-bowl-myth/index.html

Still, misinformation persists. LA sherriff (2022): https://www.dailynews.com/2022/01/12/la-county-sheriff-says-super-bowl-draws-human-traffickers/
And general LA 2022
https://www.dailynews.com/2022/01/25/la-spreads-awareness-about-human-trafficking-ahead-of-super-bowl/

Other sources indicating this trafficking does not happen:
The Polaris Project (which also goes on to state that perhaps individuals worried about trafficking ought to consider how their food and clothing is produced).
https://polarisproject.org/blog/2019/01/worried-about-human-trafficking-and-the-super-bowl-consider-your-nachos/.


ITC: https://idahoatc.org/statistics
-”80 suspicious” add in backpage.com
-increase in “sexual suggestive ad content” during superbowl week

All sorts of wild ideas of who is at risk: 
https://www.kmvt.com/content/news/Human-trafficking-easily-concealed-in-rural-Idaho-493219511.html
‘Peake said a basic sign someone may be a trafficking victim is if they are confused.’

MMIW - https://www.colorado.edu/program/fpw/2019/03/14/new-report-finds-increase-violence-coincides-oil-boom#_ftnref2

This article describes an increase in reported sexual assault, not trafficking.

And let's not forget this doozy of an anecdote:

Biz effort to reduce trafficking: https://bankingjournal.aba.com/2019/01/at-super-bowl-u-s-bank-tackles-human-trafficking/
“Ensnaring an estimated 40 million people worldwide in forced labor and sexual slavery, human trafficking shares many money laundering patterns with other criminal enterprises.” (common definition of trafficking)
“But human trafficking does have distinct red flags, notes Jim Dinkins, an SVP and financial crimes compliance operations executive at U.S. Bank who previously led investigations at the Department of Homeland Security. Perhaps the most obvious is a link to adult services advertising, “a very common indicator for sex trafficking,” Dinkins explains.”

“Starting in the summer before, U.S. Bank team members began meeting with the U.S. attorney’s office in Minneapolis. They also worked with a vendor, which provided a list of out-of-market phone numbers scraped from ads on adult services websites “that may have moved into the area around the time of the Super Bowl.” The bank could cross-reference these with its own data.” 

“The 10 day-period leading up to the 2018 Super Bowl was the most intense time of the initiative. At the ABA/ABA Financial Crimes Enforcement Conference in December, DeLuca described one of the first cases they reported: a trafficker who was on law enforcement’s radar for bad checks. He was identified through transactions on six ATM cards used around Minneapolis, and the team pulled ATM security footage. “This guy had four to five girls lined up behind him handing him money that he was putting into the ATM machine,” DeLuca said.” really...this happened just like that? It seems like a scene out a movie to illustrate what trafficking looks like.

This story both implausible, like how hollywood might represent “trafficking” in a film
(he brought 5 women with him to the ATM and had them hand him the cash. How big was his vehicle? Why bring them along rather than just get the money in advance or at least in the vehicle? And why have them hand him cash at the ATM itself, a crime, in a place well know to have camera surveillance? Honestly, are these traffickers wiley predators that everyone should watch for or bumbling criminals? 
They also indicate that sex traffickers use rideshare services so they track their victims, so US bank 

Data driven organizations: ILO


### Law Enforcement and Sex Trafficking

There is a massive focus is on ending ‘demand’ - and this focus is solely through law enforcement. As if we can arrest our way out of the demand for humanity’s “oldest profession.” If it’s true we can end demand, we might want to think about why the demand exists and address that. But wow, the prospect is far from certain. In fact, is there any evidence supporting this is possible? 


sex trafficking is clearly the focus of prosecutions:
https://www.traffickinginstitute.org/wp-content/uploads/2021/06/2020-Federal-Human-Trafficking-Report-Low-Res.pdf
2,000 prosecutions in 20 years (2000 - 2020)
‘In 2020, federal courts convicted the lowest number of defendants in human trafficking cases since 2012. In 2020, 163 DEFENDANTS were CONVICTED—an 89% CONVICTION RATE. This is a 51% decline in convicted defendants from 2019.’
-facebook is a major arena for recruitment
-they ID’ed many risk factors: substance abuse, runaways, foster care, homelessness, 


Herein lies my thesis statment: Idaho's trafficking law does nothing to stop trafficking. 

In one recent [press release](https://www.cityofboise.org/news/police/2021/september/arrests-made-in-sex-trafficking-demand-reduction-operation/), the Boise police describe a "sex trafficking reduction" operation (i.e. a sting). None of these men were charged with sex trafficking, all were charged with soliciting a prostitution.  

Idaho victim (white): 
https://www.kivitv.com/news/a-survivor-story-human-sex-trafficking-victim-in-idaho-speaks-out
‘Inside Out, Lazenko and Forrest all say that the demand for buying sex is what's driving this illegal trade.’  

And it should always be pointed out that prisons, through their used of forced labor, are one of the largest purveyors of human trafficking in the United States. 

code: https://legislature.idaho.gov/statutesrules/idstat/Title18/T18CH86/SECT18-8602/
(it bans ALL sex work as part of trafficking)

Survived and punished
https://survivedandpunished.org/

Cytoia Brown: https://www.npr.org/2019/08/07/749025458/cyntoia-brown-released-after-15-years-in-prison-for-murder

Maddesyn George and sexual assault against Native women: https://theintercept.com/2021/10/08/maddesyn-george-self-defense-rape/

‘For victims who appear to be under the influence of drugs or alcohol, she added, police often assume that the person was a willing participant rather than a victim of sexual assault.

Goodmark, the law professor, agrees. “We have spent literally billions of dollars training police and prosecutors on various forms of gender-based violence since the passage of the Violence Against Women Act and they still can’t see or won’t see victimization claims unless they come from people they deem to be credible and true victims,” she said.’

https://www.opendemocracy.net/en/beyond-trafficking-and-slavery/anti-trafficking-movement-has-left-sex-workers-behind/

#### Extent of Trafficking

BJS report:
https://bjs.ojp.gov/content/pub/pdf/htdca21.pdf

FBI reporting:
https://www.fbi.gov/investigate/violent-crime/human-trafficking

"We don't have exact numbers because human trafficking is an issue that no matter where you are it's very difficult to get accurate numbers but we do know it's happening from the specific cases. We want to make that just widespread knowledge," said President of the Idaho Coalition for Justice Kim Peake in 2018. 

https://www.eastidahonews.com/2021/05/idaho-falls-sex-trafficking-investigation-at-group-home-leads-to-rape-arrest/
‘By the time police spoke with the 17-year-old girl, she was in custody at a juvenile detention center in St. Anthony. As soon as detectives said they wanted to talk about the alleged prostitution, the girl invoked her Fifth Amendment right and did not answer questions.’

There was recently two individuals - trained, certified, vets - who have filed a lawsuit against a dairy in Idaho alleging they were lured to Idaho with promises of good jobs and then forced to do menial work while their passports and immigration paperwork was taken from them. Idaho is an agricultural state heavily dependent on migrant farm labor. It seems naive to assume these veterinarians are the only examples of human trafficking supporting agricultural labor needs in Idaho. 

Idaho ATC helped 251 people this last year https://www.kivitv.com/news/human-trafficking-is-on-the-rise-in-idaho-heres-what-you-need-to-know 

Man in Boise trafficked underage teen girl[https://www.morelaw.com/verdicts/case.asp?s=ID&d=99404]

another report: (2007-2009) https://www.unodc.org/unodc/en/human-trafficking/global-report-on-trafficking-in-persons.html
(looking at other countries that report the category of human trafficking, sex trafficking is a substantial, but not majority of trafficking). 
report mentioned large focus on sex trafficking that my underreport labor trafficking

### What Is to Be Done? 

This is what they would have you believe: 
https://www.idahopress.com/news/local/human-trafficking-happens-in-idaho-just-not-in-the-way-qanon-claims/article_d8752ef8-a841-55f4-8344-109c083539dd.html
“It isn’t just the coalition,” she said. “This is a community effort. … It’s going to take everyone.” 

A certain amount of virtue signalling is enabled by people and organization opposing sex trafficking and slavery (let me know when you meet someone advocating *for* those things), without those organizations having to addresss the harm they perpetuate.  

Lyft has partnered with anti-trafficking organization to education drivers in south Florida (https://www.lyft.com/blog/posts/best-and-lyft-partner-to-educate-drivers-on-human-trafficking-prevention). Meanwhile, rape by rideshare *drivers* still continues to be a major problem:
https://www.cnet.com/news/uber-lyft-drivers-said-to-have-sexually-assaulted-more-than-100-riders/

This is the problem, whenever the word “trafficking” is used within the context of laws or law enforcement designed to crack down on the practice, we don’t know what they mean. They can refer to the forced labor of individuals, or it can refer to consensual sex work and prostitution. 

For some, they support this point of view. But, this is far far from a settled issue. The very existence of a U.S, state and other nations permitting and regulating prositution indicates this is a viable form of work. And as many sex workers have indicated, the illegal status of their profession is what makes it dangerous. 

While this overly broad definition exist, the entire anti-trafficking movement has indicated that this is a community effort to “end trafficking”. Trainings are held, educational materials are made available, all so we, ordinary citizens, can save sex trafficking victims. Even the police are involved in this language, encouraging us to keep our eyes open and report trafficking. 

**Rights Not Rescue""
Listen to sex workers. We need to hear from waaaay more sex workers about what they want. One thing we have heard, repeatedly, is that sex work is something they want to do, and they want to do it safely. And what would make it safer? Decriminalizing prostitution, allow sex workers to place sex ads and screen customers - online and in 18+ apps. Unfortunately, very few come forward  and put their names on these demands because they become a target for law enforcement. The sex worker slogan is “rights not rescue“. https://swoptampabay.org/tag/intl-sex-worker-rights-day-march-3rd/
March 3 is the international sex workers day. 

Problems: 
conflation of trafficking victims with voluntary sex work
conflation of people seeking sex (customers) with traffickers
conflation of sexual assualt with trafficking 
understanding that homeless youth, people in foster care and lgbtq youth are at heightened risk of sex abuse, but not relation of that to people caught up in the criminal justice system
usage of stock photos of attractive, young batterred females perpetuate the  "perfect victim" narrative
language is one of healing and support, but entire enous is put on law enforcement
does not appear to understand why victims don’t contact the police

The concern about trafficking has taken a crime that is greatly worsened by:
a law enforcement and criminal justice system that disbelieves victime
a law enforcement and criminal justice system that is indifferent to the concerns of “disposable persons” - poor people, people of color, sex workers, drug users
a law enforcement and criminal justice system that criminalizes all but “perfect victims”

https://www.opendemocracy.net/en/beyond-trafficking-and-slavery/importance-unpopularity-taking-position-law-and-policy-sex-work/
‘Avoiding the issue of sex work policy weakens efforts to eradicate some of the most egregious manifestations of human trafficking because it allows governments to pretend that only traffickers cause trafficking and that states themselves play no role.’

‘https://www.opendemocracy.net/en/beyond-trafficking-and-slavery/importance-unpopularity-taking-position-law-and-policy-sex-work/.’ 
 
Trafficking victim says a support group and a job helped her the most. 

**Conclusion**

‘Our entire conception of trafficking is based on the few media stories that are filtered down to us by news editors and producers and the things that we see in movies. Trafficking by kidnap is a particular favourite – the kind that can be imagined as a crime imposed against nice, normal people who are taken from their ordinary, middle-class lives. TV writers and true crime podcasters love this, especially when sex is mixed into it.’


No one would say the sexually exploitation of children is okay, but if we fail to understand what this actually looks like and what causes it, we cannot hope to stop it. 

While these efforts are well meaning, how are they fixing the problem? Do they even understand what the problem is? 

To truly stop the harm causes by trafficking, it might mean taking up unpopular views. Or views that do enjoy quite a lot of public support, but also some public opposition - like sex work and immigration.

###############


Rape culture in Idaho:

https://lmtribune.com/northwest/eluding-justice-victim-advocates-judges-and-prosecutors-explain-why-sexual-offenders-rarely-do-time/article_8ae356c0-c698-5ab7-bf73-3ceb320c9763.html
-quotes from prosecutors, some stats

https://www.youtube.com/watch?v=hOmbLZQPzVw

https://www.ktvb.com/article/news/local/capitol-watch/idaho-priscilla-giddings-ethics-rape-complaint-lawmaker/277-2172944c-4a26-4d47-8b34-00bad9180b00

https://www.cosmopolitan.com/politics/news/a55370/idaho-sheriff-rape-kit-false-accusations/

https://www.spokesman.com/stories/2005/oct/24/lewiston-officers-say-most-rape-claims-are-false/

https://survivedandpunished.org/2021/11/23/freemaddesyn-defense-committee-statement-on-sentencing/

“abuse-to-prison pipeline” --survived and punished

Overplaying of the Duke Lacrosse incident:
https://en.wikipedia.org/wiki/Duke_lacrosse_case

OR 

rape is used to justify anti-immigrant sentiment:
https://www.preparedsociety.com/threads/5-year-old-girl-raped-by-3-muslim-refugees-in-twin-falls-idaho.29318/

