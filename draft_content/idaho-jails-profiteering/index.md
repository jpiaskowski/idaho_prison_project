---
title: "The Companies Fleecing the Families of Idaho's Jailed Populations"
date: 2022-03-01
tags: ["jail", "prison", "idoc", "GTL"]
description: "..." 
thumbnail: /images/some_iamge.jpg
draft: true
---

Let’s have a talk about commissary and commissary accounts in jails and prison. Most every jail and prison facility operates a “commissary”, basically, a convenience store for the prisoners. It has non-perishable food items, pens, paper, hygiene items (e.g. tampons, pads), and even really fancy items, like electronics (all approved). It’s like a dollar store, except everything costs much more than one dollar. Commissary stores are famously much more expensive than dollar stores, charging prices much in excess of the cost of the items sold. 

“So how much should you send to an inmate’s commissary account? Considering that they get the necessities covered, $100 is a good start. For $200 to $300, inmates can live pretty comfortably as far as prisons go.” --https://blog.globaltel.com/things-buy-prison-commissary/

“If you’re wondering how much to send your loved one for commissary, $200 is good to get them started. This will get them the basics, like shoes, a radio, MP3 player and hygiene products. With $50 from home a month, you can buy everything that you need. With $100 per month you can live very well in prison.” 
--https://prisonerresource.com/prison-life/first-day-in-prison/commissary-convict-store/
(Zoukis consulting Group, Davis CA, led by former prisoner Christopher Zoukis)

How much people spend and what they buy: https://www.prisonpolicy.org/reports/commissary.html

Main thing bought by prisoners surveyed in WA, IL and MA is food. The costs range from slightly more expensive to slightly cheaper than what is found outside of prisons. 

In Idaho…

As these costs are shifted to families, the emotional and financial toll build (https://www.themarshallproject.org/2019/12/17/the-hidden-cost-of-incarceration). 

IDOC contracts with “Access Corrections”, where you can make deposits online for a fee or at many walk-in locations (CashPayToday and AceCashExpress)

They indicate allowing free services through the mail, but in addition to the delay associated with that, there is also a complete lack of information on this process. For example, what type of payment is accepted? What information is needed to process the money? There is no example form - only a mailing addres:

Send DEPOSITS FOR PRISON RESIDENTS to:                         
      Secure Deposits-Idaho DOC                                                        
      PO Box 12486
      St. Louis, MO 63132


Kootenai County: TouchPay 

TouchPay:

3B Juvenile Detention Center
Ada County Jail
Ada County Juvenile Detention Center
Bingham Co
Bonner County Detention Center
Blaine Co
Bonneville
Canyon Co
District 1 Juvenile Detention
Elmore Co
Freemont Co
Gooding Co
Jefferson Co
Jerome Co
Kootenai County Jail
Latah Co
Madison Co
Power Co
Shoshone County Jail
Shoshone-Bannock Corrections
Southwest Idaho Juvenile Corrections
Twin Falls
Washington Co

TouchPay Holding is a service for providing funds for inmates (https://www.gettingout.com/about/). It is part of GTL Financial Services Which is owned wholly by GTL Corporation (GTL = Global Tel Link), according to the footer on their website. GTL Corporation is owned by American Securities, a private equity firm (https://en.wikipedia.org/wiki/American_Securities). 

GTL was once owned by Gores Equity, LLC (Alex Gores’s company) and sold in 2009 (and then later in 2011) to other private equity firms. 

GTL received many negative reviews on the consumer website, Consumer Affairs (https://www.consumeraffairs.com/cell_phones/global_tel_link.html). They have an average rating of 2 stars (out of a maximum of 5) and comments such as thus:  “Just as others stated in previous reviews this is a big scam.” 

Ada Co Commissary
https://adacounty.id.gov/sheriff/ada-county-jail/mail-payments/

IDOC: https://www.idoc.idaho.gov/content/prisons/resident_services/resident_accounts




This needs to be repealed:

Idaho Code §20.607 requires the Sheriff to seek reimbursement for incarceration costs from persons sentenced to jail. The cost is $25 per day, up to $500 (20 days) per incarceration.  https://legislature.idaho.gov/statutesrules/idstat/title20/t20ch6/sect20-607/


Political consequences of new districts:

https://www.idahoednews.org/news/a-new-political-map-charts-a-course-for-high-profile-statehouse-showdowns/


https://correctionalnews.com/2022/02/02/new-partnership-aims-to-help-transform-prison-conditions/

https://www.recidiviz.org/

https://apps.irs.gov/app/eos/allSearch

https://www.linkedin.com/company/center-for-justice-and-human-dignity/about/

Center for Justice and Human Dignity
https://cjhd.org/

The Sheriff App
https://thesheriffapp.com/blogDigestList/6176f8e69363775a2d4cd9a4


Touch Pay's services rely on [Twilio](https://prospect.org/justice/silicon-valley-firm-powers-predatory-prison-video-calls/), a company that has previously endorsed...(prison reform?)

