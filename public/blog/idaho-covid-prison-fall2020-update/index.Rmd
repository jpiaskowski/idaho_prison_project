---
title: "COVID-19 in Idaho's Prisons and Jail - An Update"
date: "2020-10-01"
tags: 
- IDOC 
- 'COVID-19'
- coronavirus 
thumbnail: /images/saguaro_CC.jpg
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, message = FALSE, warning = FALSE)
```

**Photo: Sagauro Correctional Center**

*Photo credit: Moss CM*

```{r}
library(ggplot2); library(dplyr); library(tidyr)

bs <- 18
source("data_import.R")

staff <- mydata %>% filter(!is.na(total_staff_covid)) 
day1 = staff %>% arrange(date) %>% pull(date) %>% first()
day2 = staff %>% arrange(date) %>% pull(date) %>% last()
dates <- paste0(format(day1, '%B %d, %Y'), " to ", format(day2, '%B %d, %Y'))

covid <- pivot_wider(summ, id_cols = population, names_from = item,
                     values_from = value) 
colnames(covid) <- c("population", "cases", "total", "deaths")
covid$percent <- covid$cases/covid$total * 100
covid$death100k <- round(covid$deaths/(covid$total/100000), 0)
```

## Death & CORONAVIRUS at the IDOC

Ten months into the pandemic, COVID-19 cases continue to spike at prisons and jails. As of November 27, 2020, there were 760 active COVID-19 cases in IDOC facilities, down from a high of 763 two days prior. IDOC staff are currently experiencing a spike in cases: 72 active infections -- a record high -- as of Nov 25, 2020. In total, there has been 294 total COVID-19 cases among staff and 2576 total cases among residents in IDOC facilities, and 132 cases among Idaho prisoners held in out-of-state contract facilities. 

For IDOC prisoners held out of state, testing has been spotty. A total of 33 COVID-19 tests were ever conducted for the 300+ IDOC prisoners at the Geo Group's Eagle Pass Correctional Facility. When those people were transferred to Core Civic's Saguaro Correctional Facility in early October, all were tested upon arrival. The result was 130 positive cases out of 311 tests, and 2 weeks later, a man died there due to COVID-19. No testing of any sort has taken place since the initial round. 

There has been at least 5 deaths at an Idaho prison due to COVID-19: 

* Frank Dawson Conover, passed away July 28th, 2020.  
* Randall Mark Osterhout, passed August 30th, 2020.  
* A man (name withheld) located in the Saguaro facility in Eloy, Arizona, passed October 14, 2020. This man's COVID-9 status was not detected until he was hospitalized.  
* A man (name withheld) housed in an IDOC facility  passed November 5th, 2020 likely due to complications from COVID-19.  
* An 88-year-old man (name withheld) passed November 13th, 2020.
* A 63-year-old man (name withheld) passed December 5th, 2020. 

*If your loved one has died in IDOC facility due to COVID-19 and you would like their name included here, please contact us.* 

It is not known how many released IDOC residents have died of COVID-19 after contracting the virus while incarcerated, but that number is not zero. 

```{r}
filter(covid, population %in% c("Idaho", "Idaho prisoners")) %>% 
  ggplot(aes(x = population, y = death100k)) +
  geom_bar(stat = "identity", width = 0.4, fill = "gray10") +
  xlab("") + ylab("deaths per 100k people") +
  labs(title = "Idaho COVID-19 Death Rates", 
       subtitle = format(day2, '%B %d, %Y')) +
  scale_x_discrete(labels = c("Idaho general population", "Idaho prisoners")) +
  theme_bw(base_size = bs-2)


ggsave(paste0("covid19_Idaho_death_rate_", day2, ".png"), width = 5, height = 5)
```

Overall, there is not a clear death rate attributable to COVID-19. Testing for COVID-19 and attribution of deaths due to COVID-19 are inconsistent across the nation and the world. In addition, death rates due to COVID-19 vary by race, age, economic status and underlying health conditions. Nevertheless, nearly [1500 prisoners in U.S correctional facilities have died due to COVID-19](https://www.themarshallproject.org/2020/05/01/a-state-by-state-look-at-coronavirus-in-prisons) as of Nov 17, 2020. If COVID-19 continues to rip through Idaho jails and prison, we can expect more deaths. Dying from COVID-19 is not a punishment any prisoner deserves. 


```{r, out.width='80%'}
g3 <- ggplot(covid, aes(y = population, x = percent)) +
  geom_bar(stat = "identity", width = 0.7, fill = "dodgerblue4") +
  ylab("") + xlab("percent infected") +
  labs(title = "Idaho COVID-19 Infection Rates", 
       subtitle = format(day2, '%B %d, %Y')) +
  theme_bw(base_size = bs-2)

g3
ggsave(paste0("covid19_Idaho_percent_", day2, ".png"), width = 6, height = 3)
```

```{r include=FALSE}
cor(mydata$total_staff_covid, mydata$total_prisoners_covid, use = "pairwise.complete.obs")
lm(total_prisoners_covid ~ total_staff_covid, data = mydata)
```

Considerably higher rates of COVID-19 are observed among prison staff and residents than the general population. The IDOC resident and staff rates are tightly linked, with a correlation of 0.94 (the highest value possible is 1 indicating complete concordance). A linear regression analysis found that for every staff member infected, nearly 10 IDOC residents are infected.  This is a revolving door of infection: staff unintentionally introduce COVID-19 to the prison population, where it spreads and infects more prisoners and more staff. Those staff also unintentionally pose an increased risk of COVID-19 transmission to their home communities. 

> ...the biggest risk for introducing Covid-19 into one of the teeming jail dorms is not newly booked inmates like this one, but rather jail employees like deputies, nurses, and kitchen staff.  Every day they come to work from possible community Covid exposure is another day they could potentially bring Covid to work with them. 
>     ---[Dr. Jeffrey Miller](http://www.jailmedicine.com/keep-covid-out-of-the-jail/)

[A previous blog post](/idaho-covid-prison/) on COVID-19 in Idaho's prisons and jails presents several reasonable solutions to alleviate this crisis.

## Perspectives from the Incarcerated

These testimonies are drawn from  the AClU's *[Voices from the Inside](https://www.acluidaho.org/en/news/stories-inside)*


### Kyle

Kyle described decent protocols at first: extra soap, masks and bleach to disinfect their block. IDOC administration was keeping residents informed of COVID-19 practices over JPay email and a decent effort from staff to follow COVID-19 safety protocols. But now? no more extra soap, no bleach, no way to disinfect common surfaces, conflicting information from the IDOC, poor ventilation and poor compliance with masks. 

>As the virus quickly spread around the country, things here at the prison stayed business as usual. The correction officers I spoke to informed me the virus was not going to affect us here in Idaho, because we are rural and Idahoans are bred from sturdier stock than citizens elsewhere in the United States. 
>     ---[Kyle](https://www.acluidaho.org/en/news/stories-inside-kyle)


>The sheer number of A-symptomatic cases here, make everyone think the virus has been over-hyped." 
 
Kyle also described some good news resulting from the pandemic:  

* better food because the kitchen had to order pre-made food rather than serving regular prison garbage.  
* case managers have brought us snacks every couple weeks (e.g. candy bar, word searches)
* phone calls cheaper by $0.03/minute    
* cheaper stamps to send mail to family   

### Scott

Scott, on missing his meds and lack of treatment for his chronic traumatic encephalopathy:

> Don’t get me wrong, I committed a crime, and I understand that there are consequences. That being said, I should still have the right to treatment for my condition. The punishment is incarceration, it should not be mistreatment and possibly ending or shortening of my life.
>     ---[Scott](https://www.acluidaho.org/en/news/stories-inside-scott)

Scott also described widespread dismissal of safety protocols and a general futility associated with the protocols due to the high-density of people at the South Idaho Correctional Institution. 

### Dana: 

>There are 59 inmates in a 50-foot by 75-foot tier without proper ventilation and we have no hygiene products. The prison has been negligent/careless and not even following their own rules of mandatory masks.

>I have heard of only one death at the facility and only 200 cases, but we are not well informed about anything. We are always on lockdown without any recreation since June 20th and all programming and visitation have been canceled. I haven’t had contact with my support system. This has started to give me anxiety and depression.
>     ---[Dana](https://www.acluidaho.org/en/news/stories-inside-dana)

### Austin

>But, ultimately I believe everyone not convicted and sentenced to death should be released, since death is a real possibility to all inmates with the Covid-19 in the institutions. I wish everyone luck in these days of uncertainty and crisis.
>     ---[Austin](https://www.acluidaho.org/en/news/stories-inside-austin)


## Updated Charts of COVID-19 Cases in IDOC

```{r, out.width="100%"}

g1 <- staff %>% rename('total staff cases' = total_staff_covid, 'daily active cases' = daily_staff_active) %>% 
  ggplot(aes(x = date, y = `total staff cases`, group = 1,  
             label = date)) +
  geom_line(col = "#E1650B") +
  geom_segment(y= 0, aes(xend = date, yend = `daily active cases`), col = "#519B9A") +
  xlab("") + ylab("confirmed COVID-19 cases") +
  labs(title="IDOC Staff COVID-19 Cases", subtitle = dates) +
  geom_point(size = 2.5, alpha = 0.7, col = "#5E6265") +
  theme_bw(base_size = bs)

g1

ggsave(paste0("covid19_IDOC_staff_", day2, ".png"), width = 7, height = 3.5)
```

*The vertical blue bars indicate the daily active cases, and the plotted points connected by a red line are the total COVID-19 cases among IDOC staff. Daily active cases among staff or prisoners were not tracked in the IDOC tally until June 14th, 2020.* 



```{r, out.width="100%"}
totes <- mydata %>% select(total_prisoners_covid, total_EaglePass_covid, total_Saguaro_covid) %>% 
  mutate(final = rowSums(., na.rm = T)) %>% pull(final)

dailies = mydata %>% select(daily_prisoners_active, daily_Saguaro_active_asymptomatic, daily_Saguaro_active_symptomatic) %>% 
  mutate(final = rowSums(., na.rm = T)) %>% pull(final)

prisoners <- cbind(mydata, totes, dailies) %>% filter(!is.na(total_prisoners_covid)) 
day1 = prisoners %>% arrange(date) %>% pull(date) %>% first()
day2 = prisoners %>% arrange(date) %>% pull(date) %>% last()
dates <- paste0(format(day1, '%B %d, %Y'), " to ", format(day2, '%B %d, %Y'))

g2 <- prisoners %>% rename('total prisoner cases' = totes, 'daily active cases' = dailies) %>% 
  ggplot(aes(x = date, y = `total prisoner cases`,  
             label = date, label1 = `total prisoner cases`)) +
  geom_line(col = "#E1650B") +
  geom_segment(y= 0, aes(xend = date, yend = `daily active cases`, 
                          label2 = `daily active cases`), col = "#519B9A") +
  xlab("") + ylab("confirmed COVID-19 cases") +
  labs(title = "IDOC Prisoner COVID-19 Cases", subtitle = dates) +
  geom_point(size = 2.5, alpha = 0.7, col = "#5E6265") +
  theme_bw(base_size = bs)

g2
#ggplotly(g2, tooltip = c("label", "label1", "label2"))

ggsave(paste0("covid19_IDOC_prisoners_", day2, ".png"), width = 7, height = 3.5)
```
*Same legend as previous figure (see above). Figures indicate positive COVID-19 cases at IDOC facilities and Contracted Facilities.*

## Idaho Jails

Jails have dramatically reduced their reporting, to the detriment of community health and planning. As a result, we do not have a clear sense of COVID-19 infections in jails. 

In Twin Falls, money from the [Federal CARES Act](https://home.treasury.gov/policy-issues/cares), designed to "provide fast and direct economic assistance for American workers and families, small businesses, and preserves jobs for American industries", is being used to expand the their jail [source](https://www.boisestatepublicradio.org/post/twin-falls-county-uses-cares-act-funds-pay-jail-expansion#stream/0). 

Nine people held in the Elmore County Jail are [suing that jail](https://www.idahostatesman.com/news/coronavirus/article245831700.html) for generating an unsafe environment due to overcrowding and insufficient sanitation protocols for COVID-19. They are requesting to be released. The Elmore County Jail has made any information public regarding COVID-19 infections rates in the jail or among jail staff. 
