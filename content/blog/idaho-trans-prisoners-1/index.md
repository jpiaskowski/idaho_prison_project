---
title: "Idaho Trans Prisoners: In Their Own Words (Part 1)"
date: 2024-07-23
tags: ["IDOC", "transgender", "healthcare", "HB 668"]
description: "Transgender people currently incarcerated in Idaho talk about their experiences and how they are dealing with the anti-trans bill, HB 668"
thumbnail: /images/trans_flag.jpg
---

*Image: Torbak Hopper/Flickr*

### Update

The Idaho Supreme Court has [suspended enforcement of HB 668](https://idahocapitalsun.com/2024/08/02/federal-judge-temporarily-blocks-law-for-people-in-idaho-prisons-on-hormone-therapy/) for trans prisoners receiving treatment for gender dysphoria while the [ACLU-led lawsuit](https://www.acluidaho.org/en/press-releases/aclu-idaho-scores-major-victory-recent-lawsuit-robinson-v-labrador) proceeds. 

### Background

In the 2024 legislative session, Idaho passed [HB 668](https://legislature.idaho.gov/sessioninfo/2024/legislation/H0668/) banning the use of state funds for any gender affirming care. The language specifies that:

> Public funds shall not be used, granted, paid, or distributed to any entity, organization, or individual for the provision or subsidy of any surgical operation or medical intervention described in section 18-1506C(3), Idaho Code, for purposes of altering the appearance of an individual in order to affirm the individual's perception of the individual's sex in a way that is inconsistent with the individual's biological sex regardless of whether the surgical operation or medical intervention is administered to a minor or an adult"
> [HB 668 text](https://legislature.idaho.gov/wp-content/uploads/sessioninfo/2024/legislation/H0668.pdf) 

The bill contains further provisions that such care may not occur on state property, these procedures are not tax deductible and Idaho Medicaid will not reimburse such care. Violations of this law are considered 'misuse of public funds' under Idaho Code [18-5702](https://legislature.idaho.gov/statutesrules/idstat/title18/t18ch57/sect18-5702/), that provides charges of misdemeanors or felonies, depending on the responsibilities held by the charged Idaho employee. 

While devastating for anyone receiving hormone therapy or other transgender-related healthcare, this bill is particularly harmful to transgender prisoners in Idaho, who are forced to depend on healthcare from the state. The [ACLU of Idaho](https://www.acluidaho.org/) is currently pursuing [litigation against the State of Idaho](https://idahocapitalsun.com/2024/07/02/idaho-officials-sued-by-inmates-over-law-blocking-public-funds-for-gender-care/) on behalf of three transgender prisoners. 

### Alex (he/him)
*Pocatello Women's Correctional Center* 

I started HRT (hormone replacement therapy) back in 2014, as soon as it was available here [at the IDOC]. I knew I wasn't what I saw in the mirror at a very young age. When I was around 7 yrs old I hated when my mom would dress me up girly. Even into high school my mom would make me dress feminine, I feel like she knew, and thought she could change me. She would go so far as to wake up early before I'd leave for school and if she didn't like what I was wearing she'd make me change. This led to a lot of self harm and suicide attempts. I did what I could to appease her even though it made me hate my body even more.

My transgender journey before I was able to start HRT wasn't good at all. A lot of self mutilation, I didn't even want to acknowledge my chest area and did all I could to hide it. Once I was able to start my shots, and finally be me, who I know I've always been, I changed. Everyone around me saw the change, I've become more confident, happy, positive, it's just made a lot of positive changes. Even though I still have parts of my body I hate, I love the changes that my body has made. I love my facial hair! My voice change, everything. 

My family has been very supportive, even my mom. Once she saw how happy I became she backed me 100%! Same with my friends, everyone has been very supportive. The support from the [IDOC] staff here has been amazing. Our warden used to be the head clinician here, she's the one that made all of this possible. Our old warden is the person who voted to let me get top surgery. They are great about pronouns to. There are a handful of [IDOC] residents who refuse to respect my pronouns, but for the most part everyone is very respectful.

I barely learned about Idaho House Bill 668 earlier this month thru someone else, then I was talked to by the provider here and given the details. So yes, they said as of July 1st the state will no longer pay for any gender dysphoria care, and the hormone shots can no longer he done on state property. IDOC is still trying to figure out what they will do. They are going to begin tapering us off HRT July 1st. They were going to pay for our injections thru a different funding, but because the Bill says no GD care can be done in federal facilities, their hands are tied. Even though I was approved for top surgery, thanks to the Bill, the state won't pay for my transport. IDOC hasn't prepared, and only now have begun to try to figure out what will be done. 

I get my injections on Thursdays, I asked how long our taper would be they said 90 days. A clinician came to see me to see how I was with all of this, not that it helped any. I have a lot of anxiety knowing things within my body will change and I can't prevent them. They had tried decreasing my dose a few years back and it put me on a manic roller coaster. My depression was thru the roof, I didn't know if I wanted to cry or be angry, I had a short fuse, knowing that I'm going to have to go thru all of that again but 10× worse is stressful. I don't want to revert back to hating my body, and having it betray me. 

I've been on HRT for about 8 years now. I'm terrified of what will come with having to quit my injections. The thought of menstruating again gives me immense anxiety. I know I will go back to hating myself. I would prefer that we are allowed to continue our HRT, especially since quitting will bring on a lot of heart issues. 

When I was given my recent testosterone injection [in June] and was told how short of time the taper would be and also when my last injection would be given I felt a lot of anxiety. Even though I knew this would happen, in the back of my mind I had hopes that someone would put a stop to it, that it wouldn't come to this point. I don't understand how society took so many steps forward toward acceptance, yet Idaho has decided to take leaps back. What kind of world do we live in when a person can be told who they are is simply a "perception" of how they see themselves. When people see me, they see me as I am, a man. HRT allowed my outside self to match what my soul and spirit has always been. Now, because of where I am and the mistakes I have made, it allows a state to say I must be who they "perceive" me to be? House Bill 668 has become a death sentence to many, apparently Idaho is just fine with that.

I want the public to know how much pain it's causing that the state is saying I can't be me, that I have to be who they feel I should be. 

### Allison (she/her)
*Idaho State Correctional Institution*

I had been put on hormone therapy in 2020 I was on hormone therapy consistently for 3 years got out on parole then was abruptly unable to take them because I couldn't afford them due to being laid off at my job. 

I was diagnosed with Gender Dysphoria in September of 2019. I have been receiving hormone replacement therapy since February of 2020. I have always known that I was different since the age of about 5, but always known that I am female. I didn't come across the term "transgender" till about age 18.

So I don't really go by the trans label, I view myself as a woman with a birth defect that I'm trying to fix, so to me I'm already a woman. My journey has had its ups and downs. Most of my family is supportive, but I also lost a few family members due to my transition, the reason that they were not supportive is due to religious beliefs, they have told me that they will never accept me and never accept me as a woman. The rest of my family loves me as who I am a woman, sister, and daughter.

Some support comes from other [IDOC] residents but there are more that don't respect us at all including staff members, the residents will sexualize things and attempt to sexual harass us and sometimes sexual assault us; they can also be violent towards us. When it comes to staff, they will disrespect up by mis-pronouning us, and when we ask for help to move out of a risky situation, they don't help us find a different living situation.

I have such a severe case of Gender Dysphoria that I have in the past pre-hormones have tried to self castrate myself 14 times in my lifetime but had stopped trying after hormones because they helped. Now that gender affirming care is being prohibited, I have started to get the feelings of hopelessness about my transition and getting back into the thoughts of self castration.

[In response to questions about how IDOC is treating her gender dysphoria] Hormone replacement therapy medications such as Estrodial, Spironolactone, Progesterone, Finesteride, and Prenatal Vitamins. I have also requested gender confirmation surgery for the last 3 years and all I have been told is that I'm on the assessment list but still have not been seen, I got told I was on the assessment list a year ago.

I learned about Idaho house bill 668 when the others trans women here brought it to my attention. The IDOC is not ready for this bill at all. They sent out a memo for a meeting that was held on 6-20-2024 between transgender residents and 10 administration staff that all ranged between security staff, medical staff, and mental health staff. They told us that we would be tapered off of our hormones,m but didn't know when and for how long and that all gender affirming care would stop 7-1-2024. They told us that maybe mood stabilizer medication could be used to replace them if we were feeling Dysphoria. They basically told us that they didn't know what was going to happen in the future due to Idaho house bill 668. All of the transgender community is being affected by Idaho house bill 668 including myself.

I have not been coping very well [since the meds were cut off starting July 1st], I have already felt the affects of testosterone in my body. My mental health issues of gender dysphoria are getting back to where they were pre-hormone therapy. I'm in a lot of emotional pain and anguish because of all of this. It makes me feel hopeless about my future and being my self, I don't feel like a person anymore. I feel like an object that people just want to control.

I personally plan to fight this bill and for my rights as not only a trans woman but also a a human being of our society and for the rights of all trans people in our society. this will impact me personally because its very degrading to all trans people and make me feel like I'm not a person, it makes me have the thoughts of self castration again, it also has the high possibility of bringing forth serious health risks from being taken off of hormone replacement therapy especially if you've been on them for a while. I would prefer the bill not be in place and that it gets overturned so that way all trans people can receive gender affirming care so we can feel like ourselves, for myself I would like to feel normal like the woman I am and not like a freak because of my defect.

I would like the public to know that trans people are also people and strive for equality and have done so for a long time, we deserve the same respect that all people should get. I would the fellow trans people out there fighting along side with me to know that we are all in this together and that we can only get things done together as a unified community and not to back down just because things get tough. I would like to send out a message to all trans people and trans activists and advocates out there that if they would like to get ahold of us in here to work together in this issue here is my info: Allison Muscarella, #121886 on [Jpay](https://www.jpay.com).
 
To anyone out there who feels like there alone out there dealing with these issues please know that your not, we all must rely on each other for support.

***Part 2 of These Interviews Will Be Released Soon.***

### How to Help

1. Contact your [local state representatives](https://legislature.idaho.gov/legislators/whosmylegislator/) about the harm wrought by HB 668 and your personal opposition to this bill. 

1. Donate to the[ ACLU of Idaho](https://www.acluidaho.org/), who is currently litigating this bill. 

1. Write to a prisoner! Hearing from folks on the outside who care can be immensely encouraging for those locked up. [Write a Prisoner](https://writeaprisoner.com/) is a good way to find incarcerated people looking for a pen pal. 
