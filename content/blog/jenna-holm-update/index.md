---
title: "The Failures of Bonneville County in the Case of Wyatt Maser"
date: 2022-08-07
tags: ["Bonneville County", "sheriff", "Jenna Holm"]
description: "When Sheriff's deputy Wyatt Maser was killed by another officer in the line of duty, the county tried to blame the citizen they were assisting and never held the responsible officer accountable."
thumbnail: /images/bonneville_intersection.jpg
---

**Photo: Eric Grossarth, [East Idaho News](https://www.eastidahonews.com/2021/06/internal-investigation-reveals-factors-into-death-of-deputy-wyatt-maser/)**

### Background

In the early morning of May 18, 2020, Bonneville County Sheriff's Deputy Wyatt Maser lost his life while responding to a call in Idaho Falls. He and another officer arrived to assist a motorist, Jenna Holm, after she was in a single-car crash on a rural stretch of road. They arrived to find Holm in distress. While attempting to bring her into custody, another officer, Sergeant Randy Flegel, arrived driving at high speeds and hit Deputy Maser with his patrol vehicle. Maser died at the scene. Holm was charged with manslaughter in connection with his death. The charge was [dismissed](https://www.eastidahonews.com/2021/09/judge-drops-manslaughter-against-woman-accused-of-causing-deputys-death/) by Idaho District Judge Dane Watkins September 23, 2021. 

Maser was 23 years old at the time of his death. He had a wife and 8 month old baby at the time of his death. 

![](maser.jpg)
**Deputy Wyatt Maser** 

I previously wrote about the [history](https://www.idahoprisonproject.org/blog/jenna-holm/) of charging people for accidental cop-on-cop shootings because those individuals were involved in the incident in which the police officers died. It is s a very troubling trend that avoids any accountability of police officers in their own missteps and completely upends the lives of the people accused (if they survive the encounter). In Holm's case, the charges were dismissed because there was no evidence or allegations that Flegel and Holm conspired together to commit a crime, but not before she spent 19 months in jail unable to post bail. 

### Latest Updates

Holm was still facing charges of aggravated assault from the May 20, 2020 incident. She pled guilty to two misdemeanors in a plea deal and was sentenced to time served. In her statement to the court, Holm apologized to the family of Maser for her role in his death. “I’m going to do better in my life,” she said. Holm is currently in the Bonneville County Jail awaiting trial for a drug-related offense. 

![](jenna_2022.jpg)

**Jenna Holm.**  *(Photo: [Johnathan Hogan, Post Register](https://www.postregister.com/news/crime_courts/jenna-holm/image_0152aa60-9257-5f72-aa02-2fab306e923b.html))*

In their victims´ statements, Maser's family is unhappy with the responses from the Bonneville County Sheriff's office, the Bonneville County District Attorney and Holm. They felt she should not have been allowed to plead to a misdemeanor. Sandra Arnold, Maser's mother stated: “The choices you made May 18, 2020, destroyed the life of my son". They were also deeply disappointed with the decision to not charge Randy Flegel. 

> Having the actual person who killed my husband, Sgt. Flegel, not have a single repercussion and still be able to put on the same uniform that my husband is buried in and continue to work for the sheriff’s office is appalling to me.

--- **Paige Maser (widow)**

### Bonneville County Prosecutor and Police Learn Little from the Experience

Whatever her mistakes on May 20th, 2020 as well before and after that, it is not Holm's fault that Maser died. Her drug use or lack of cooperation is not blame for Wyatt Maser's death, as Maser's family believes. There is no way she could have known this tragic set of events would result from a drive home one very late night/early morning. Furthermore, that she is currently awaiting trial for drug possession is not evidence of a lack of a remorse. It is evidence of how difficult it is to break from addiction. She did make an apology to the family in court; nevertheless, the extent of her remorse for Maser's death is known only to her. 

The Bonneville County Prosecutor continues to contend that Holm and only Holm is responsible for Maser's death. In fact, the prosecutor's office seems to think there was nothing wrong with the conduct of the Sheriff's officers that day, despite an [internal investigation](https://www.eastidahonews.com/2021/06/internal-investigation-reveals-factors-into-death-of-deputy-wyatt-maser/) identifying several mistakes. This report from the investigation was briefly made public before being sealed by an Idaho court. 

The Bonneville Sheriff's office never held Randy Flegel responsible for his actions to any extent. Holm was always the lead suspect in Maser´s death. Flegel was immediately cleared of any wrong doing by the [Eastern Idaho Critical Incident Task Force](https://www.eastidahonews.com/2021/04/what-is-the-eastern-idaho-critical-incident-task-force/), which was never revisited (as far as we know) by the Bonneville DA. Given the history that police officers have of [not properly investigating fellow officers](https://www.denverlawreview.org/dlr-online-article/policeinvestigatingpolice) and the [close relationship](https://digitalcommons.law.uidaho.edu/cgi/viewcontent.cgi?article=1031&context=idaho-law-review) between the prosecutors and law enforcement, this is not surprising.  

In a rather [incredulous statement](https://www.postregister.com/news/crime_courts/jenna-holm-sentenced-to-time-served-in-case-connected-to-death-of-deputy/article_3f6cc900-822f-5824-a6d7-7f1a8300a355.html), Bonneville County Prosecutor Randy Neal told Holm in court that the actions of law enforcement saved her life that night. Sorry, what? What was she saved from? This person needed a tow truck, a mental health intervention and substance abuse treatment. Instead she got screaming, swearing cops ("Fucking tase her" was shouted), a taser, and while incapacitated, a sheriff's deputy was killed. She was having a mental health break likely spurred on by drug usage, but the police made the situation infinitely worse. It is not unreasonable to state that everyone would have been better off if the sheriff's office had not been present at all that morning. Bonneville County is either unable or unwilling to make this admission, a clear refusal to engage in public accountability. 

Two years after the tragic and preventable death of Wyatt Maser, has Bonneville County learned anything? We may never know. 
