---
title: "Gentle Introduction to Abolition"
date: 2025-01-05
tags: ["prison abolition"]
description: "Prison abolition is a vision for a just society so that harm is prevented or dealt with in it in a way that does not increase violent behavior."
thumbnail: /images/abolition_women.png
---

*Image: [Isabel Tran](https://cityonahillpress.com/2021/02/27/visualising-abolition-puts-the-abolition-feminism-movement-into-perspective/)*
                                
I’d like to take some time in 2025 to reset and refocus on prison abolition. This is a blog focused on prison abolition, not prison reform. This point of view - abolishing prisons - usually surprises a few folks who view it as unrealistic. However, most people do not understand what prison abolition is. And most folks are so conditioned to the omnipresent role of police, prisons and jails in our lives that most of us struggle to envision a world without those things. But, prisons are not inevitable, or even natural structures. They are others ways to handle crime, or better stated, *harm*. 

### Basic Facts

**1. First, prison abolition is most certainly about ending prisons, jails and policing.**

There is ample evidence that prisons are ineffective ways to handle crime and most importantly, are not an inevitable part of society. Prisons have not always existed! Not all countries use prisons to handle harm, and certainly no nation incarcerates its citizens like the Unites States does. 

**2. Second, we aren’t dismantling prisons tomorrow.**

Seriously, that is not happening anywhere! Don’t freak out. 

**3. Prison abolition does not mean no consequences or accountability for bad behavior that brings harm to others.**

Nor do justice alternatives (e.g. transformative justice) mean forcing people to participate in mediation with their abusers or meaningless apologies to be forced out of people. These processes of repairing harm are complex and voluntary. Prison abolition envisions an array of alternatives for handling harm, and--most importantly--*preventing harm*. 

**4. Prison abolition is a vision for a just society so that harm is prevented or dealt with in it in a way that does not increase violent behavior.**

How do we -- as the state, as communities, as society – support people so their needs are met? How do we build structures that prevent harm? What is needed? **The focus is intentionally on harm not crime.** All sorts of crazy things are crimes, but not all harmful actions are crimes. In Idaho, it’s a [felony for a state employee to give any instructions on how to obtain an abortion](https://legislature.idaho.gov/statutesrules/idstat/title18/t18ch87/sect18-8705/), but it’s [legal to use ‘faith healing’ on sick children](https://www.idahostatesman.com/opinion/opn-columns-blogs/article286299395.html), even if that results in their (preventable) death.  

## Abolition as a Philosophy

If abolition sounds incredibly broad, you have understood it correctly. 

At its heart, abolition is a political philosophy that posits (1) prisons make us less safe and we are better off without them; and (2) people are inherently good and will tend towards supporting each other when their needs are met. That doesn’t mean abolitionists have a naive Buddy-the-Elf vision of how humanity behaves (You need to have seen [Elf](https://en.wikipedia.org/wiki/Elf_(film)) to make sense of this pop culture reference) or have not directly experienced violence in their lives. The prison abolition movement is largely developed and led by Black women, many of whom have directly experienced interpersonal violence at the hands of friends, family, strangers and police. It is precisely this lived experience that leads all of us to embrace abolition as the most realistic way to reduce harm in our lives. A world without harm is not possible, but we can embrace policies that reduce violence, not that those that worsen it.

For an outstanding and engaging introduction to abolition, watch this [video](https://youtu.be/hBGU6bwB60k?feature=shared) by Olayemi Olurin. It’s only 13 minutes and absolutely spot-on. 

> We can’t just use prison to disappear people into prison for years where they endure abuse and violence, dump them back into their under resourced communities and expect that problems are solved. 
> Olayemi Olurin (slightly paraphrased)

I’m not going to write a fully comprehensive guide to abolition because many others have already done this (see below for some suggestions!). But, if you are concerned about how to reduce violence and crime, consider learning more about this political philosophy (again, see book recommendations below). 

Abolition is also framework to understand what changes or reforms to undertake and which proposed changes increase inequality and expand our prison system. There are many organizations in Idaho whose work supports an abolitionist vision:  organizations that provide food, housing and resources to those that need them; organizations that seek to reduce violence and help people and communities thrive. For 2025, I’d like to highlight those abolitionist efforts in Idaho. We are a largish state with a low population that is separated by some dramatic geology and landscapes, so it’s not easy to forge and maintain community connections that nourish the varied people who live here. I want to give accolades and flowers to as many community centered organizations doing good work as possible this year.

### Books to Learn More About Abolition

<img src="images/becomingAbolitionists.jpg" alt="becoming abolitionists book cover" style="width:150px;height:auto;">
*[Becoming Abolitionists](https://www.penguinrandomhouse.com/books/675803/becoming-abolitionists-by-derecka-purnell/)* by Derecka Purnell. Part educational, part personal memoir, this book charts Ms. Purnell’s journey to becoming a prison abolitionist. She provides insight into how her thinking evolved: why are there cops in schools and what are they doing? What happens when you call the police? If you can’t call the police, whom do you call? What should we do about murderers and rapists?  Deeply personal and moving, it’s a great introduction to abolition.

<img src="images/NoMorePolice.jpg" alt="becoming abolitionists book cover" style="width:150px;height:auto;">
*[No More Police: A Case For Abolition](https://thenewpress.com/books/no-more-police)* by Mariame Kaba and Andrea Ritchie. A comprehensively researched and referenced text that is also alive with personal stories of work the authors have put towards building alternatives to policing.  

<img src="images/PrisonObsolete.jpg" alt="becoming abolitionists book cover" style="width:150px;height:auto;"> 
*[Are Prisons Obsolete](https://www.penguinrandomhouse.com/books/213837/are-prisons-obsolete-by-angela-y-davis/)* by Angela Davis. A classic abolitionist book arguing that yes, prisons are not needed and our experiment with mass incarceration should end. 

<img src="images/unbuildWalls.jpg" alt="becoming abolitionists book cover" style="width:150px;height:auto;">
*[Unbuild Walls](https://www.haymarketbooks.org/books/2213-unbuild-walls)* by Silky Shah. A primer on how immigrant enforcement detention is an extension of the US policing and prison system and why abolitionist visions must include how the immigrants are criminalized simply for being migrants. 

