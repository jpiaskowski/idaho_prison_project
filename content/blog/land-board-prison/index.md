---
title: "Bad for Idaho: the Land Board's Proposal to Build a Prison"
date: "2020-08-15T23:39:06-06:00"
tags:
- Idaho Land Board
- prison construction
thumbnail: /images/IMSI.jpg
---

The Idaho State Board of Land Commissioners, charged with managing state lands to support public institutions, [has proposed to build a prison on state lands as an investment](https://idahonews.com/news/local/could-idaho-use-endowment-money-to-build-a-new-prison-facility-the-idea-is-being-vetted), which will be leased back to the State of Idaho. Proponents indicate this will relieve overcrowding and will be cheaper than Idaho building its own prison. They also indicate humanitarian concerns from the emotional cost to families due to housing Idaho inmates at a private prison in Texas that can be solved with a new prison.

The Idaho Prison Project has initiated [a petition on change.org opposing the this proposal](http://chng.it/6ZqK7BR9).


### Petition Text

**We, the undersigned of this petition, oppose the construction of a prison on Idaho State Lands.**

From a financial standpoint, this plan is not optimal management of Idaho’s public lands for generating revenue. The construction of a permanent building on state-owned lands requires a long-term lease from Idaho Department of Corrections. Prisons and prisoners should never be viewed as an “investment opportunity.” Ideally, prisons should be viewed as rehabilitative institutions. A successful criminal justice system should aim for fewer inmates over time, not more. As the nation grapples with criminal justice reform, Idaho’s criminal justice system should not be linked to maintaining a minimum prisoner count in order to fulfill a lease obligation.  Tying incarceration to financial returns is morally offensive and, as shown in Idaho’s earlier experiment with private prisons, results in rampant human rights abuses. 

Idaho could seek to address its prison overcrowding problem by reducing its prison population rather than building more costly prisons. Idaho has one of the highest incarceration rates, 734 individuals per 100,000 people ([2018 data](https://www.prisonpolicy.org/global/2018.html)). This is greater than the U.S. average of 698. A large portion of Idaho inmates are serving lengthy sentences for drug use crimes or drug-related parole violations. These behaviors are better addressed through drug treatment programs, not incarceration. Furthermore, it is deceptive to cast building a prison as “humanitarian” because inmates will be housed closer to their loved ones. While we appreciate the acknowledgment of the emotional burden of housing prisoners so far away, building another prison is not a compassionate, humanitarian action. 

In their [October 2018 Report](http://idahocfp.org/new/wp-content/uploads/2018/10/ICFP-2018-Idaho-Prisons-Fact-Sheet-1.pdf), The Idaho Center for Fiscal Responsibility detailed how incarceration consumes increasing portions of the Idaho state budget, and how other states have been successful in saving state monies by reducing the prison population without endangering public safety.  In Idaho, corrections spending rose 204% between 1992 and 2017, while crime rates dropped across Idaho. To put this in perspective, public school education only rose by 84% in the same time frame. Idaho land management should not prioritize prison construction above their mandate to manage lands for public institutions, such as public schools. There are numerous  other opportunities for the Idaho Department of Lands to diversify its portfolio such as commercial real estate, timberlands, agriculture lands, and other investment opportunities identified by the Idaho Land Board’s [Reinvestment Subcommittee](https://www.idl.idaho.gov/about-us/land-board/). They do not need this project to meet their statehood mandate that state lands  “be managed in perpetuity as a trust for the beneficiary institutions.”

[**Sign The Petition!**](http://chng.it/6ZqK7BR9)
