---
title: "Important Notice Regarding Future Communications With Idaho Prisoners"
date: 2022-04-29
tags: ["IDOC", "jpay", "COVID-19"]
description: "As COVID restrictions lift, COVID-related protocols also lift, including one that offered affordable communication between Idaho prisoners and their loved ones."
thumbnail: /images/jpay_app_reviews.png
---

**Photo:** *Reviews of JPay app available in the Apple app store.* 

Article by Patrick Irving

Reprinted from [Book of Irving](https://bookofirving82431.com/2022/04/14/important-notice-regarding-future-communications-with-idaho-prisoners/) with permission of the author. 


On 4-5-2022, the Idaho Department of Correction delivered the following message to their resident population: 

> MESSAGE FROM THE IDOC: 
>
> Dear IDOC Resident,  
>
> Idaho Gov. Brad Little has announced that he will end his declaration of the public health emergency related to COVID on April 15, 2022. Effective May 1, 2022, the IDOC, in connection with ICSolutions, will end the promotional items that have been extended during the COVID pandemic.
>
> Sincerely,
>
> Idaho Department of Correction


What this means is, as of May 1, the following will no longer be offered to Idaho prisoners, their friends and their families: 
  * one (1) free monthly video connect session
  * two (2) free JPay stamps, Free Reply Wednesdays
  * two (2) free weekly phone calls
  * reasonably priced JPay e-stamp packages.

The price of the JPay stamps used to send emails are expected to be raised back to the exorbitant amount of approximately $0.40 apiece, when purchasing 50 or more at time.

Some of you may remember that IDOC worked with JPay prior to the pandemic to prevent people from purchasing stamp packages at the rate negotiated by Washington State Department of Corrections. While Washington State Corrections has always offered stamp packages at a fraction of the price IDOC has negotiated, it was implied by the Department at the time that seeking to communicate with loved ones imprisoned at an affordable rate is a manipulative act.

Others may remember the following excerpt from the message Director Josh Tewalt issued on September 9, 2020:

> Costs for Calls/JPay: Speaking of rates, I’ve had some folks asking if the cost reductions were about to expire soon and the answer to that is no. We negotiated a permanent rate reduction.

Out of concern that the information provided by Director Tewalt was unreliable, we asked an IDOC contract manager this week if JPay stamps will return to their previous prices. Associate #D40 responded: “Yes. I am working to see if anything can be done. Please stay tuned…”

Those that wish to play it safe are encouraged to stock up on e-stamps at the current price of 60 for $10.

As of May 1, should IDOC fail in their alleged attempt to renegotiate with JPay, prices are expected to jump to 50 for $20.

It is currently unclear if phone prices will return to pre-pandemic levels as well (a 33% increase). An answer to this question will be posted on my [blog](https://bookofirving82431.com/) as soon as possible.

Click [here](https://bookofirving82431.com/2022/04/06/first-amend-this-an-idoc-newsletter-apr-22//#charges) to learn more on how the Idaho Department of Correction allows prison profiteers to charge their prisoners’ loved ones at the highest rate in the nation.

---------------

Jails and Prisons routinely charge their residents [exorbitant fees](https://www.prisonpolicy.org/blog/2019/09/11/worststatesphones/) to send emails, make phone calls and otherwise stay in touch with their loves ones. These costs far exceed the actual cost to deliver the item, resulting in massive windfalls to the contracting companies and occasionally the facilities themselves. In Idaho, county jails and the state prison system have contracted with providers to provide phone calls, email capability and commissary items, charging fees as high as 15%. Together, these become the one of the [hidden costs of incarceration](https://www.themarshallproject.org/2019/12/17/the-hidden-cost-of-incarceration) absorbed by the families of those incarcerated. 
