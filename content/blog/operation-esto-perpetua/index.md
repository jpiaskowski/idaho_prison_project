---
title: "Governor Little's Operation Esto Perpetua is More Drug War Nonsense"
date: 2023-02-06
tags: ["Operation Esto Perpetua", "fentanyl", "substance abuse"]
description: Idaho's "Operation Esto Perpetua" is poised to repeat the same mistakes made in the drug war with fentanyl. With a focused on law enforcement, expect more mass incarceration under this endeavor. 
thumbnail: /images/fentanyl_test_strip.jpg
---

**Fentanyl test strip.**  *[AP Photo: Mark Lennihan](https://www.apimages.com/search?query=mark+lennihan&st=kw&ss=10&toItem=15)*

#### Idaho Could Do Something About Substance Abuse, but, Instead We Get “Operation Esto Perpetua”

tldr;

* "Operation Esto Perpetua" was an embarrassing and unprofessional public relations tour of the state that did not result in usable recommendations on how to address Idaho's fentanyl crisis.  
* Idaho provides minimal funding for drug treatment and mental health resources, but pours money into prisons and policing as a solution to the fentanyl crisis. 

* Policy changes Idaho needs to reduce fentanyl overdose deaths: 

  1. adequately fund substance abuse treatment and recovery resources
  1. legalize and distribute fentanyl (test) strips
  1. educate public on Narcan usage to reverse an overdose
  1. don't pass a bunch of bills throwing people in prison for drug use!

------------------------

>Meth and fentanyl are the most serious and growing drug threats in Idaho, and there is a direct tie to the loose border with Mexico. Recognizing there is much more we can do to make Idaho’s communities safer, I asked law enforcement, lawmakers, cities, counties, tribes, families, and the public to come together in a new way with one goal – to meaningfully reduce the flow of fentanyl and meth the State of Idaho.

--Brad Little ([press release](https://gov.idaho.gov/pressrelease/gov-little-highlights-back-the-blue-investments/) dated April 4, 2022)

And so kicked off “Operation Esto Perpetua”, in what is [described](https://gov.idaho.gov/pressrelease/gov-little-highlights-back-the-blue-investments/) as “the Governor’s plan to help to combat the smuggling of fentanyl and other high-profile narcotics into the state due to the ongoing border crisis.”  

## Fentanyl in Idaho

Drug abuse has long been a problem in Idaho. According to the [Idaho Drug Overdose dashboard](https://www.gethealthy.dhw.idaho.gov/drug-overdose-dashboard), figures for overdoses indicates 846 emergency room visits for opioid overdoses in 2020 and 1,075 visits in 2021 (a 27% increase). Figures for 2022 appear to be tracking closely with previous years.  There has been an increase in overdose deaths: in 2021 there were 343 statewide, compared to 287 in 2020 (a 20% increase). 

## Operation Esto Perpetua

Operation Esto Perpetua takes its name from the Idaho State motto, *Esto Perpetua*, or may it endure forever. 

While the Governor’s office has presented this as [his initiative](https://gov.idaho.gov/operation-esto-perpetua/), the funding is actually provided to the Idaho State Police (ISP). Judging from their presentation made to JFAC last legislative session, this was not intended to be a joint operation between the Governor’s office and ISP, nor was there any component to hold public meetings. In the presentation made by ISP head Kedrick Wills to [JFAF February 2nd in 2022](https://legislature.idaho.gov/wp-content/uploads/sessioninfo/2022/standingcommittees/220208_jfac_0800AM-Minutes.pdf), this is all that was said about the request for Operation Esto Perpetua: 

> The division also requests two FTPs and an ongoing $250K in General Funds for two specialist positions to combat the smuggling of fentanyl and other narcotics....We need an additional workforce to combat the rapid and overwhelming quantities of this deadly drug. The additional resources provided by Operation Esto Perpetua will fund ISP investigators in Lewiston and Pocatello and the associated overtime costs for these statewide.

--ISP Director Kendrick Wills to JFAC (2022)

The [ISP appropriation](https://legislature.idaho.gov/sessioninfo/2022/legislation/H0750/) for fiscal year 2023 (beginning July 1st of 2022) reflects their request: $250,000 from the general fund to support two full-time positions, one located in Lewiston and the other located in Pocatello. Nothing in that request indicates the series of hearings held later that spring of 2022. 

Since the Governor’s office got involved, this Operation has taken a significantly different tact. The “Esto Perpetua” page on the Governor’s [website](https://gov.idaho.gov/operation-esto-perpetua/) indicates that this project is a “new strategy to do more to protect our communities. “Operation Esto Perpetua” brings together law enforcement and communities in new ways, to turn the tide and protect our children.” They also indicate it is [funded by the Legislature](https://gov.idaho.gov/pressrelease/gov-little-launches-operation-esto-perpetua-to-fight-idahos-growing-drug-threat/). Since funding from the Legislature for OEP did not begin until July 1, 2022 (the start of the new fiscal year), they would not have been able to use dedicated OEP funds to support their travel and public meetings prior to that date (although meetings were held beginning in April). 

### OEP Panel

Thus far, the Governor's office has convened a panel consisting of law enforcement and "citizens" and held 4 public meetings across the state. The citizen's panel is entirely composed current or formerly elected officials. 

**Law Enforcement Panel**  

* Idaho State Police: Colonel Kedrick Wills  
* Idaho Chiefs of Police Association: Pocatello Police Chief Roger Schei  
* Idaho Sheriffs Association: Canyon County Sheriff Kieran Donahue  
* Governor’s Office of Drug Policy: Marianne King  
* Fraternal Order of the Police: Boise Police Detective Mike Miraglia  

**Citizens Action Group** 

* Idaho House of Representatives member: Speaker Scott Bedke (R-District 27)   
* Idaho Senate member: Senator Abby Lee (R-District 9)   
* City official: Coeur d’Alene Mayor Jim Hammond (R)   
* County official: Custer County Commissioner Wayne Butts (R)  
* Tribal member: Shoshone-Paiute Tribe Chairman Brian Thomas (D)  
* Public member #1: Dana Kirkham, former mayor of Ammon (R)  
* Public member #2: Luke Malek – Action Group Chair, Former State Representative (R-District 4a)   
* *Ex officio*: Kedrick Wills  

Representatives from Governor Little's office were unable to describe the process used to choose panel members (personal communication with M. Hardy). No open process appeared to be conducted. 

Missing from the citizen's panel are:

* More Tribal representation. The largest Indian Tribe, The Shoshone-Bannock that has [5900 enrolled members](http://www.sbtribes.com/) was not included, nor was any Tribe from Northern Idaho included, a list that includes the Niimiipuu ([3500 enrolled members](https://nezperce.org/about/)), the Coeur d'Alene Tribe ([2200 enrolled members](https://www.cdatribe-nsn.gov/our-tribe/history/)). 
* Representative from the Idaho Department Behavioral Heath
* Substance abuse treatment professionals

### OEP Meetings

This panel has held 4 public meetings events in Idaho cities to gather input from law enforcement and citizens about “their loved ones’ experiences with fentanyl and meth.” [According the Coeur d’Alene Press](https://cdapress.com/news/2022/apr/05/fight-your-doorstep/), the goal of these meetings is for the citizens panel to produce a report that the law enforcement panel will use for crafting action items. Meetings were held in Coeur d'Alene (April 4, 2022), Weiser (April 27, 2022), Pocatello (May 9, 2022), and Twin Falls (May 11, 2022). Additionally, there was a private meeting with the [Bonneville County Sheriff](https://bonnevillesheriff.com/news/2022-04/fentanyl-in-bonneville-county/) that was not open to the public. 

The meeting's stated purpose was to gather feedback from Idaho citizens on how fentanyl issue affect their communities. The citizens panel was in attendance to listen; and Governor little occasionally opened the meeting by discussing the growing threat of fentanyl to Idaho. 

#### Feedback

These meetings were heavily dominated by law enforcement, who provided the majority of the feedback. At the first meeting (held in Coeur d'Alene), Keiran Donahue, a member of the law enforcement panel and the sheriff of Canyon County, a jurisdiction located 400 miles South of Coeur d’Alene), said “[it's time] show people how dirty and evil this drug is” [source: Cda Press](https://cdapress.com/news/2022/apr/05/fight-your-doorstep/). Kootenai County Sheriff’s officer Brett Nelson said that Kootenai County’s border with Washington state is a high drug trafficking corridor. Coeur d’Alene Police Chief Lee White had two recommendations: hire more police officers and create additional diversion alternatives to jail for first-time offenders. 

Two round tables were held in Twin Falls in mid-May also invited a heavy presence from law enforcement and the criminal justice system. Twin Falls County prosecutor Grant Loebs urged the panel to consider mandatory minimums for “dealing a large quantity” (source: [Times-News](https://magicvalley.com/news/local/government-and-politics/threatening-the-heart-of-who-we-are-facing-fentanyl-together-with-operation-esto-perpetua/article_ba0f8a74-d16d-11ec-8073-1701be124bc4.html). Minidoka County Prosecutor Lance Stevenson also supports mandatory minimum as well as life in prison for “high-volume dealers”, asserting that these long sentences are a deterrent. He was also an advocate of very high sentences for fentanyl trafficking. Pocatello police chief Roger Schei indicated that fentanyl is now being “laced” into marijuana (Note: fentanyl-contaminated marijuana this is widely understood among medical professional to be a [myth](https://www.medicalnewstoday.com/articles/fentanyl-laced-marijuana#what-is-it)). Bannock County prosecutor Janice Price wants mandatory minimums for fentanyl possession.

There was some citizen feedback. Tamara Ashley, a social worker in the Duck Valley Indian Reservations, indicated that inadequate staffing and overloaded caseworkers have resulted in high burnout and turnover as a result (source: [meeting notes](esto_perpetua_statewide_meeting_transcripts.pdf)).  

A [press release](https://gov.idaho.gov/pressrelease/gov-little-seeks-input-from-idahoans-on-meth-fentanyl/) dated May 6, 200 indicated that the public could send comments to the governor's office, but the deadline was 7 days after this announcement (May 13, 2022).  

The full meeting notes are available [here](esto_perpetua_statewide_meeting_transcripts.pdf). According to the Governor's office, all meetings were recorded and these notes are transcribed from those recordings. 

### Recommendations from the Panel

Unsurprisingly, most of the recommendations focused on law enforcement. There was a desire to provide more resources to "border counties" (likely those which border Oregon or Washington), do something about the "loose drug laws" in those states, improve communications between law enforcement agencies, launch an "overdose tracker" dashboard (unclear how this is different from the [IDHW overdose tracker dashboard](https://www.gethealthy.dhw.idaho.gov/drug-overdose-dashboard) that has been active since October, 2020), and engage in some unspecified educational activities. 

### What is wrong with these recommendations? 

These recommendations seemed to toggle between actual needs and political posturing. Regardless, these are unlikely to fix issues with fentanyl in our communities and will worsen existing problem facing Idahoans.  

#### They are expensive

Imprisoning people is [expensive](https://prisonsreview.com/how-much-does-it-cost-to-keep-someone-in-prison/), between $31,000 - 60,000 annually per person, and in Idaho, consuming an [ever increasing](https://idahofiscal.org/fact-sheet-on-idaho-prisons/) portion of Idaho's budget. 

#### They are ineffective

Long prison sentences are not effective deterrents for crime; neither is the death penalty an effective deterrent, according the [National Institute of Justice](https://nij.ojp.gov/topics/articles/five-things-about-deterrence). Substance abuse disorder has causes (e.g. poverty, mental health, domestic abuse, lack of support), and throwing users in prison does not help the issue. Furthermore, imprisonment of any kind [does not actually reduce](https://www.pewtrusts.org/en/research-and-analysis/issue-briefs/2018/03/more-imprisonment-does-not-reduce-state-drug-problems) drug usage.

#### They worsen the problem

Prison based solution often worsen substance abuse by (1) [trapping people in poverty](https://www.apa.org/pi/ses/resources/indicator/2019/04/incarcerated-women); (2) causing [long-term familial instability](https://nap.nationalacademies.org/resource/18613/dbasse_089046.pdf) and are particularly [harmful to children](https://www.pewtrusts.org/en/research-and-analysis/blogs/stateline/2016/05/24/having-a-parent-behind-bars-costs-children-states); (3) further stigmatizing people with substance abuse disorder. Such [stigmatization](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4079128/) prevents rehabilitation and reintegration of individuals in a community. 

#### They are predicated on misinformation

* As mentioned above, long prison sentences are not a crime deterrent. 
* Drug laws in Washington and Oregon are not to blame for trafficking of fentanyl. This is not legal anywhere because it is highly lethal! 
* Biden's border policies are not to blame for fentanyl trafficking in the U.S. Many Republicans including Governor Little, have publicly blamed undocumented migrants for tracking vast amount of fentanyl into the U.S. that is killing citizens. [According to the DEA](https://www.dea.gov/sites/default/files/2021-02/DIR-008-21%202020%20National%20Drug%20Threat%20Assessment_WEB.pdf), most fentanyl is being trafficked through legal ports of entry via U.S. citizens. [Here](https://twitter.com/ReichlinMelnick/status/1531818600000016385?s=20&t=l-YuYZiKA66P9n--rQM1Jg) is a summary of those findings. 
* Regarding the body scanner: Most drugs are introduced into jails and prison via [staff who work there](https://www.forbes.com/sites/walterpavlo/2021/09/30/corrections-officers-often-key-to-contraband-introduced-into-prison) (honestly, just google "drug smuggling prison"and many examples of prison workers smuggling in drugs will pop up). 

## What Really Needs to be Done

Reducing drug abuse is [complicated](https://europepmc.org/article/med/28226333) and requires a multi-tiered strategy. Contrast these recommendations from 'Operation Esto Perpetua' to those of the [Idaho Opioid Misuse and Overdose Workgroup](https://odp.idaho.gov/wp-content/uploads/2021/06/2017-2022-Idaho-Opioid-Misuse-and-Overdose-Strategic-Plan-2021-Update.pdf), which has comprehensive suite of actions to combat opioid usage in Idaho. This working group is composed of governmental agencies, municipalities, medical services and other organizations with a stake in substance abuse disorder. Their recommendations include affordable drug treatment, evidence-based education, and harm reduction. 

We cannot arrest our way out of this crisis. Arresting users exacerbates the problem by generating the conditions that lead to substance abuse disorder in the first place. And targeting the supply is not enough. The fentanyl crisis is the latest iteration of a substance abuse epidemic that began with the fraudulent marketing OxyContin (*Dopesick*, 2018 by Beth Macy and *Empire of Pain*, 2021, by Patrick Raden Keefe).  

### Education program to prevent first time usage

There is a considerable body of [research](https://nida.nih.gov/publications/drugs-brains-behavior-science-addiction/preventing-drug-misuse-addiction-best-strategy) supporting effective educational programs for adolescents to prevent substance abuse. These approaches are also supported by [Drug Policy Alliance](https://drugpolicy.org/issues/real-drug-education), which is committed to "comprehensive, cost-effective approach[es] to secondary school drug education and school discipline that is all about helping teenagers by bolstering the student community and educational environment" ([source](https://drugpolicy.org/sites/default/files/DPA_Beyond_Zero_Tolerance.pdf)).

Unfortunately, one of the most widely implemented substance abuse educational programs, DARE, is not among those considered effective prevention programs. Many studies have demonstrated that DARE has little to no effect in preventing substance abuse in the short term, and has no long term effects ([Becker 1992](https://pubmed.ncbi.nlm.nih.gov/1484326/), [Ennett 1994](https://pubmed.ncbi.nlm.nih.gov/8092361/), [West 2004](https://pubmed.ncbi.nlm.nih.gov/15249310/), [Faggiano 2014](https://pubmed.ncbi.nlm.nih.gov/25435250/), [Pan 2009](https://pubmed.ncbi.nlm.nih.gov/19440283/), [Clayton 1996](https://pubmed.ncbi.nlm.nih.gov/8781009/), [Tremblay 2020](https://pubmed.ncbi.nlm.nih.gov/32769198/) among [other studies](https://pubmed.ncbi.nlm.nih.gov/?term=%22drug+abuse+resistance+education%22+effectiveness)). The extent of DARE programs across Idaho is not know. The [DARE website](https://dare.org/idaho/) lists several sheriff's office across Idaho as participating programs. Operation Esto Perpetua made several vague references to 'education' without indicating curricula or even the target audience. Perhaps schools and professional educators are better trained and equipped to engage in educational endeavors than Sheriff's offices. 

### Harm Reduction 

Harm reduction is a suite of policies that at its core, seeks to reduce people dying from overdoses rather than focusing on stopping people from using drugs. It is [challenging work](https://whyy.org/segments/doing-more-good-than-harm-conflicting-feelings-on-the-frontlines-of-harm-reduction/), but it is effective in saving lives. Idaho Department of Health and Welfare (IDHW) does run a harm reduction project thatcan be reached at 208-991-4574. 

As members of the public, here is we can support harm reduction: 

#### Reverse an overdose

* Know the signs of an overdose and how to respond to an overdose. Some signs of an overdose: 
  * small, constricted “pinpoint pupils”
  * falling asleep or losing consciousness
  * slow, weak, or no breathing
  * choking or gurgling sounds
  * limp body
  * cold and/or clammy skin
  * discolored skin (especially lips and nails)

For more information, visit [Idaho Deparment of Health and Welfare](https://healthandwelfare.idaho.gov/services-programs/behavioral-health/overdose-response). 

![](narcan_cda.jpg)

*Photo: Billboard outside of Tensed, Idaho*

Overdoses can be reversed with Naloxone (trade name: [Narcan](https://www.narcan.com/)). Having this available is something we can all do! Narcan is available at pharmacies across the U.S. (even in Idaho!), and this should be paired with training on how to recognize an overdose and how to administer narcan. Contact the IDHW for [training](https://healthandwelfare.idaho.gov/services-programs/overdose-response) on how recognizing and reversing an overdose. 

#### Make fentanyl test strips legal

Make [fentanyl test strips](https://www.webmd.com/mental-health/addiction/fentanyl-testing-strips) available (so users can evaluate if something else they bought is cut with fentanyl). These are not currently legal in Idaho, but other [states have legalized fentanyl strips](https://whyy.org/articles/fentanyl-test-strips-decriminalized-pennsylvania-law/). Idaho legislature: you should legalize fentanyl strips!
  
***ALL*** of these approaches result in reduced overdoses and deaths. Harm reduction does work! 

### Affordable drug treatment

In fiscal year 2023, the Idaho Legislature funded [16 positions](https://legislature.idaho.gov/sessioninfo/2022/legislation/S1384/) supporting drug treatment. These programs are [funded](https://legislature.idaho.gov/wp-content/uploads/budget/committee/jfac/2021/D4.Thursday,%20February%204/04.Substance%20Abuse%20Services.pdf) almost entirely by Federal dollars, along with with a small number of dedicated funds (~10% of the total funds) and zero general funds as the Governor's office and JFAC (the Idaho Legislature's Joint Finance-Appropriations Committee) have explicitly told IDHW no to ask for general funds. In that budget is funding for "peer recovery centers", highly effective tools for helping people recover from substance abuse disorder. They are largely staffed by volunteers, each with a paid director and some operating costs. Given the extent of the fentanyl crisis in Idaho, why are these peer recovery centers so vastly underfunded? It is kind of people to volunteer and their service is much appreciated, but, is this something that can truly be relied on for the long term? 

> We know that peer-based recovery is highly effective in helping people stay in recovery.
> [IDHW to JFAC, 2022](https://insession.idaho.gov/IIS/2022/Joint%20Finance-Appropriations%20Committee/220208_jfac_0800AM-Meeting.mp4)

### For the Idaho Legislature: Don't make things worse 

* Guarantee addiction recovery medicine will be administered for those in Idaho's jails and prisons. Jail personnel have [not honored](https://www.ama-assn.org/delivering-care/population-care/why-denying-addiction-treatment-jails-prisons-inhumane) medical advice and dispensed medical prescriptions. [Treating withdrawl](https://www.ama-assn.org/delivering-care/population-care/why-denying-addiction-treatment-jails-prisons-inhumane) is essential to help people recover and to protect their health. Jails and prisons are legally responsible for the health and safety of those incarcerated. 
* Provide drug treatment options for people imprisoned in Idaho who are seeking it.
* Do not pass harsh laws that separate people from their families. 
* Adequately fund mental health resources. 
* Adequately fund support for victims for domestic violence 

## Final Thoughts

In 2020, U.S. Congress convened the Commission on Combating Synthetic Opioid Trafficking in 2020. In their [report](https://www.rand.org/content/dam/rand/pubs/external_publications/EP60000/EP68838/RAND_EP68838.pdf), they highlighted the limits to interdiction: 

> Other federal guidelines have focused on reducing supply of prescription medications for acute or chronic pain. Although these well-intended policies have sought to reduce misuse and diversion of prescription opioids, constraints on supply have failed to reduce the number of overdoses. Reducing the unnecessary prescribing of medications that result in [opioid use disorder] is a necessary part of a holistic framework for reducing demand for drugs by limiting the exposure of medications. However, absent any commensurate increase in [opioid use disorder] treatment options and utilization, restrictions on prescription opioids have instead coincided with an increase in heroin use and overdose. Some people with [opioid use disorder] switched to heroin when obtaining prescription opioids became more difficult. Others switched to heroin because it costs less than diverted prescription opioids. But the increase in the number of overdose deaths only accelerated with the arrival of illegally manufactured synthetic opioids, such as fentanyl, and the speed with which they replaced heroin in drug markets.

They also include a list of "pillars" of what an ideal U.S. fentanyl response should like. This includes interdiction and several public health aims:

* Support evidence-informed efforts to reduce substance misuse and progression to substance-use disorder.
* Expand access to evidence-based treatment.
* Enhance evidence-informed harm-reduction efforts.
* Take efforts to promote recovery from substance-use disorder

Missing from the list includes (1) monitoring migrants entering from Mexico and (2) instituting severe punishments for individuals with substance abuse disorder. 

We all want fentanyl under control. No one wants to hear of another premature death due to overdose or a life destroyed by addiction. Most Americans would rather see substance abusers treated for their addiction rather than punished. [We do not want](https://www.pewresearch.org/politics/2014/04/02/section-1-perceptions-of-drug-abuse-views-of-drug-policies/) mandatory minimum for drug offenses. Yet, Idaho State lawmakers are posed to repeat mistakes of the 1980s and 1990s by passing very punitive laws for fentanyl users. 

> One of advocates’ greatest fears about the legislation is that it will expand the imprisonment of drug users, and contribute to the socioeconomic instability that often fuels addiction and abuse in the first place.”
> [Tara Law](https://finance.yahoo.com/news/state-laws-treating-fentanyl-crack-134448927.html)

My hope is that Idaho's legislators will avoid this fate. 
