
library(googlesheets4)
gs4_deauth()

# read in IDOC data
link = "https://docs.google.com/spreadsheets/d/12D-nVm5by7fGaHMARnu_M5tN8olNo1TbNYHcPXPN8vg/edit?usp=sharing"
sheet1 = "IDOC_covid_tally"
sheet2 <- "IDOC_summary"
mydata <- read_sheet(link, sheet = sheet1, na = "NA")
summ <- read_sheet(link, sheet = sheet2, na = "NA")



# get Idaho COVID data
# library(httr)
# library(jsonlite)
# url <- "https://api.covidtracking.com/v1/states/id/daily.json"
# records <- GET(url)
# rec_text = content(records, as = "text", encoding = "UTF-8")
# covid_id = fromJSON(rec_text)
# 
# covid_id$Date <- as.Date(paste(substr(covid_id$date, 1, 4), substr(covid_id$date, 5, 6), 
#                        substr(covid_id$date, 7, 8), sep = "-"))


