---
title: "Idaho Trans Prisoners: In Their Own Words (Part 2)"
date: 2024-07-25
tags: ["IDOC", "transgender", "healthcare", "HB 668"]
description: "Transgender people currently incarcerated in Idaho talk about their experiences and how they are dealing with the anti-trans bill, HB 668"
thumbnail: /images/trans_painting.png
---

*Image: trans artist [Kalki Subramanium](https://www.kalkisubramaniam.com/)*


### Update

The Idaho Supreme Court has [suspended enforcement of HB 668](https://idahocapitalsun.com/2024/08/02/federal-judge-temporarily-blocks-law-for-people-in-idaho-prisons-on-hormone-therapy/) for trans prisoners receiving treatment for gender dysphoria while the [ACLU-led lawsuit](https://www.acluidaho.org/en/press-releases/aclu-idaho-scores-major-victory-recent-lawsuit-robinson-v-labrador) proceeds. 

### Background

In the 2024 legislative session, Idaho passed [HB 668](https://legislature.idaho.gov/sessioninfo/2024/legislation/H0668/) banning the use of state funds for any gender affirming care. The language specifies that:

> Public funds shall not be used, granted, paid, or distributed to any entity, organization, or individual for the provision or subsidy of any surgical operation or medical intervention described in section 18-1506C(3), Idaho Code, for purposes of altering the appearance of an individual in order to affirm the individual's perception of the individual's sex in a way that is inconsistent with the individual's biological sex regardless of whether the surgical operation or medical intervention is administered to a minor or an adult. 
>
>  ( [HB 668](https://legislature.idaho.gov/wp-content/uploads/sessioninfo/2024/legislation/H0668.pdf)) 

The bill contains further provisions that such care may not occur on state property, these procedures are not tax deductible and Idaho Medicaid will not reimburse such care. Violations of this law are considered 'misuse of public funds' under Idaho Code [18-5702](https://legislature.idaho.gov/statutesrules/idstat/title18/t18ch57/sect18-5702/), that provides charges of misdemeanors or felonies, depending on the responsibilities held by the charged Idaho employee. 

While devastating for anyone receiving hormone therapy or other transgender-related healthcare, this bill is particularly harmful to transgender prisoners in Idaho, who are forced to depend on healthcare from the state. The [ACLU of Idaho](https://www.acluidaho.org/) is currently pursuing [litigation against Idaho](https://idahocapitalsun.com/2024/07/02/idaho-officials-sued-by-inmates-over-law-blocking-public-funds-for-gender-care/) on behalf of three trans prisoners. 

### Alice Rayne Harlow (she/her)
*Idaho State Correctional Institution*

I have been transitioning for the last 15 years or so but I knew who I was since I was a child, about 5 or 6. I have been on hormones for the last 10 years and have changed immensely both physically and mentally. I never understood the issues I would run into by being trans, and I always considered it a calculated risk because it would be worth my happiness no matter what happened. I don't talk to anyone in my family save my mother and step dad, no one else is okay with my life choices and honestly, if that is all it takes to lose someone then they are not people I wanted in my life anyway, as callous as that sounds, it helps me through the pain of losing those I care about.

I have always been trans but I didn't fully "come out" until I came to prison. I was in a band for years as the front man and we did fairly well for ourselves but not as well as I would've liked. I was miserable. it took a while for my mom to come around and really she didn't fully come around until she saw how I was happier being me. Then she really accepted it.

There are a lot of truths that you need to face about yourself when you decide to come out as trans and you have to take a look in the mirror and be ready to answer those questions with complete honesty and there are time when you question your answers even. Is this really the right move for me? Is this really what I want in life? Am I ready for the changes to my body that will never go away? Can I deal with being ostracized for being me? I would like to say that for most of us those questions are answered with a resounding "I'm ready" but others have a fear that freezes them and turns them to stone, so fully coming out is a scary thing.

I have been on hormones for over 10 years now and now that the house bill is specifically targeting gender affirming care I am losing access to my mental health medications because the hormones are not simply medical but they also help ameliorate the mental health symptoms of gender dysphoria. Being on them doesn't fix everything but it helps me feel closer to how I feel like I should feel if I was born the right way.

I was paroled from here December 15, 2023 and the same day I left prison, I was asked to leave the halfway house I was at because I was trans. Bethel Ministries is a very faith-based program, and they felt that having me there went against what they believed. I went to Alabama because that is where my wife (who is also trans) is and I got in trouble for absconding (which is true) but I asked for help and received none and was condemned to the streets of Boise for who I am. 

This issue [HB 668] I feel is bringing [the IDOC trans community] is closer together and we are sticking together for the most part for the first time since I have been here but the support I need personally is that which fixes the problem because a hug doesn't fix the fact I am losing my hormones and expected to not react to it, if I do react in a negative way I get in trouble and risk not going home to my wife. In truth, I am terrified. I am on an open-dorm tier where I have no privacy and am constantly sexually harassed and I have asked for help but have been told to deal with it. I am trying to do the right things but honestly I had better results when I wasn't following the rules. 

IDOC didn't respond with much sympathy or anything other than cool regard. We had a meeting with mental health, medical and security staff to try and figure out how we, as the trans inmates, can get the help we need and the mental health side and medical seemed to be fairly perceptive but the warden left within 15 minutes of the meetings start and the captain and LT left soon after and didn't seem to want to hear what we had to say. So honestly it seems like the state as a whole could care less either way. they don't really care if we are the victims, they care that we continue breathing that we may continue to pay our debt to society, it doesn't much matter how that happens.

Medical is tapering us off our hormones little by little they are trying to prolong it as much as they can but they basically said they are subject to get a felony charge if they continue to provide our hormones past that so their hands are tied.
Mental health is under security so they can't move us at all. All they can do is ask security if they can move us, again cool regard.

I feel targeted and ostracized by this bill. Normally I don't pay too much attention to what goes on around here because if I get too invested, I won't go home to my wife and she is more important than anything else in this world. That being said this is something that requires my attention because it targets me and my people specifically. I am almost at the point where I am done asking for help, nobody is listening. How bad does it have to get before the powers that be hear us? Do I need to start cutting again or attempt to self castrate before they understand the gravity of the situation? How bad does it have to get?

We are human and we deserve the right to live our lives and get the care we need just like everyone else does. We are easy targets for the weak minded and the bullies in the world because so many people ar uneasy with our presence that they are able to stir the pot and gang up on us and make us victims. 

My message for other trans women out there is this...We are not victims, we are queens and queens bow to no one. We are our worst enemies and we will always break ourselves more than anyone else ever will. I think it is time for us to band together against the hate and prejudice and fear and show the world that we are not out to get anyone, we just want our fair shot in life. Put gender aside for a moment, we are all human and we make mistakes and we thrive to be better so how can one be above another for simply choosing a different life path or life goal? Why do we, as humans, need to perpetuate the hierarchical belief that one person is better than another? It is time to stand tall and be the kings and queens we were born to be. It is time to stop being scared and take pride in who you are because we won't go anywhere and the more society tried to break us, the stronger and louder we will be. You are not alone in how you feel, not anymore.

### Tylynn (she/her)
*Idaho State Correctional Institution*

I started HRT [hormone replacement therapy] while I was still out in the community in December of 2020 through Dr. Alviso, the same specialist we see in here. Prior to that I had my G.D [gender dysphoria] assessment done June 2019 while I was at Idaho State Correctional Center (ISCC).I knew that I preferred to be q girl since I was 12. Back then I had no idea what "transgender" and "gender dysphoria" was. When I talked to a trans woman at ISCC I told her my story and I realized that what I had been feeling for 14 years was gender dysphoria, I was 22 at the time. 

My journey has been good in some ways and worse in others. Its helped me bring out the female in me who felt she's had to hide in order to be accepted by others. It was until recently that I really began to feel confident in expressing my self as a female regardless of the opinions the others may have. Embracing my trans self both as an identity and expression has made me feel more at ease with myself. 

My family has been supportive even though they may struggle with this identity/physical change. I've been lucky to not have a family member or friend withdraw support. I've received a lot of support from clinical and medical staff along with a small number of correctional staff. It seems though that administrative staff don't want to help the trans residents or even recognize that there are trans woman at this facility. 

On 08/19/2021 I had a disagreement with my bunk mate involving me being transgender. It escalated to the point where a couple correctional officers got involved. By standard operating procedures (SOPs), they should have immediately moved one of us to a different unit or in administrative segregation. However that was not the case. A couple if hours later the inmate attacked me and the injuries were numerous. I was left on the floor of the unit bleeding with a broken orbital bone, two burst blood vessels in an eye, a concussion, and multiple lacerations. The first correctional officer that was involved did a tier check and when he seen me on the floor severely bleeding he stepped over my body continuing his tier check and didn't call code as he should have done per SOPs. I ended up receiving surgery on the broken orbital bone and seven stitches in my bottom lip. Many months later I was talking with the CO [corrections officer] that rode in the ambulance with me. He told me that my face was so swollen that I quit breathing 3 times. The paramedic told the CO that if I quit breathing again he was going to do a tracheotomy so I could breath. The officers were not held accountable for allowing me to beaten and not calling code when I was obviously very injured. The other inmate was charged with aggravated battery. The case was taken to a jury trial where the jury were in favor of the State [the defendant was found guilty].

I learned of this bill [HB 668] back in early or mid May. I honestly don't know how IDOC is preparing for this. There was a conference type meeting with trans residents. Staff included Deputy Warden Of Security (can't remember his name), Captain Gibney, Idaho Chief Psychologist Dr. Walter Campbell, Clinical Supervisor Jeremy Clark, and a few more. We asked questions and brought up concerns/issues in regards to the house bill and other ones. The main one being housing as trans residents are to be housed together according to SOPs. The Deputy Warden gave no comments/in put and left the meeting within the first half hour. The Captain said he was more than willing to help us with our housing issues yet that remains to be seen. 

Living with a non-dysphoric resident makes our dysphoria worse and heightens risk for trans woman to be raped. In my opinion IDOC is doing very little while mental/medical staff is actually being more supportive of trans residents. Every trans resident is being impacted. The HRT is more than a medication that changes body appearance. Seeing the outward changes of our body helps alleviate our gender dysphoria, so its also a medication that helps with mental stability.

[In response to question, do you feel supported by other IDOC residents] Yes! Some of the other trans woman and I don't talk or hang out, especially outside of the support group. I hope they know that I'm still here for them and am being as supportive as I can. I'm using the support network that I have here [to cope with removal of HRT]. Along with that I am using positive coping skills and activities such as drawing, writing, going to the trans pride group, and interacting with people who are pro LGBTQ+.

### Conclusion

HB 668 includes this preamble:

> The Legislature finds that...[gender affirming healthcare and surgeries]...carry substantial risks and have known harmful effects, including irreversible physical alterations and, in some cases, sterility and lifelong sexual dysfunction.
>
> ([HB 668](https://legislature.idaho.gov/wp-content/uploads/sessioninfo/2024/legislation/H0668.pdf) )

Evidence for this is thin, but, there are known harmful effects to denying treatment and healthcare to people suffering from gender dysphoria. The [American Medical Association](https://www.ama-assn.org/press-center/press-releases/ama-reinforces-opposition-restrictions-transgender-medical-care) and the [America Academy of Pediatrics](https://www.aap.org/en/news-room/aap-voices/why-we-stand-up-for-transgender-children-and-teens/) supports providing gender affirming care to transgender and gender divergent people of all ages. This is the recognized standard of medical care for gender dysphoria among medical experts in this field. 

### What Can You Do

1. Contact your local state representative and senator about the harm wrought by HB 668 and your personal opposition to this bill. 

1. Donate to the ACLU of Idaho, who is currently litigating this bill. 

1. Write to a prisoner! Hearing from folks on the outside who care can be immensely encouraging for those locked up. [Write a Prisoner](https://writeaprisoner.com/) is a good way to find incarcerated people looking for a pen pal. 
