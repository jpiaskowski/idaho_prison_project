---
title: "Carceral Feminism is One Heckuva Drug"
date: 2022-10-09
tags: ["Carceral feminism", "abolition", "justice", "von Ehlinger", "sex offenses"]
description: "The lengthy sentence handed to Aaron von Ehlinger is a time to examine what we want the justice system to do for us."
thumbnail: /images/m_revolving_door_cropped.jpg
---


*Image: [Gabi Campanario](https://linktr.ee/gcampanario) / [Seattle Times](https://www.seattletimes.com/opinion/from-prison-to-college-to-success/)*

#### *[Updated October 9, 2022](#update)*

There was great celebration after convicted rapist Aaron von Ehlinger was [sentenced last week to 20 years](https://idahocapitalsun.com/2022/08/31/former-idaho-rep-aaron-von-ehlinger-sentenced-to-at-least-8-years-in-prison-for-rape/) in prison for raping a legislative intern while he was a sitting Idaho legislator 

What could be a greater symbol of justice to victims of sexual assault? Or so I was told in many jubilant reactions.

### Recap of the Facts

In March of 2021, while representing the District 6 (R-Lewiston), the then 39 year-old von Ehlinger raped an 19 year-old legislative intern in his home. He was [found guilty of rape](https://idahocapitalsun.com/2022/04/29/former-idaho-representative-found-guilty-of-felony-rape-in-trial-involving-legislative-intern/) in April, 2022. One year prior to his trial, he was [censured by the Idaho House Ethics committee](https://blog.idahoreports.idahoptv.org/2021/04/29/ethics-committee-votes-to-suspend-censure-rep-von-ehlinger/) for his action with the intern and facing expulsion, von Ehlinger resigned from the House of Representatives. Priscilla Giddings, a sitting representative for District 7A (R-White Bird), [publicized the victim’s name](https://apnews.com/article/government-and-politics-73627bd31fdc3d54a7b9c001324e9615) (hereafter called “Jane Doe”), photo and address to Giddings facebook followers. At von Ehlinger’s House ethics hearing, Jane Doe was [ambushed by supporters of von Ehlinger](https://www.idahocountyfreepress.com/news/i-forgive-you-an-in-depth-look-at-the-von-ehlinger-hearing-testimony-of-jane/article_1a21a034-aa03-11eb-beec-779d11221a8e.html), who took her photo. Current and past photos of an underage Jane Doe were [shared widely across Idaho right wing media sources](https://apnews.com/article/idaho-government-and-politics-ec5f5d78b3caba0c4f0eb4788d6a2026) without her consent. [Priscilla Giddings was censured](https://idahocapitalsun.com/2021/11/15/idaho-house-votes-49-19-to-censure-rep-priscilla-giddings/) by the Idaho House ethics committee for doxxing Jane Doe, resulting in her expulsion from the [House Commerce and Human Resources Committee](https://www.kmvt.com/2021/11/15/idaho-lawmakers-censure-priscilla-giddings/). 

## The Lasting Effects on Jane Doe

In addition to the trauma she experienced from the rape itself, the [damage from the bullying, harassment and intimidation from von Ehlinger supporters](
https://www.idahopress.com/news/local/it-s-been-a-struggle-jane-doe-prepares-for-jury-trial-against-former-state-representative/article_87957dca-112e-5718-a3f5-8b9956b795b5.html) to Jane Doe has been extreme. She describes being unable to have a social media presence or even have utility bills in her own name, lest people find her. [She was unable to finish testifying](https://idahocapitalsun.com/2022/04/27/i-cant-do-this-alleged-victim-in-von-ehlinger-rape-trial-cuts-testimony-short/) at von Ehlinger’s trial due to the overwhelming stress and trauma it was causing her. She also described being shunned by the political establishment in Boise including the Governor’s office, who refused her annual request for a photo with Governor Little in the days after her allegations broke in the news media. Jane Doe was - and it still - a young adult just starting out in life. Her stated intent of meeting with von Ehlinger was to network and advance her career; these career aspirations have been derailed, perhaps permanently for her. This person has been deeply harmed by von Ehlinger, Priscilla Giddings, Emri Moore, and countless other people who cruelly smeared her to advance their own political interests. 

What have we done for her? What have we done to help her get the help she needs? What have we done to prevent these things from happening to future victims?

## Carceral Feminism and Mass Incarceration

As mentioned, there was much celebration in response to von Ehlinger’s 20 year sentence, of which a minimum 8 years must be served before he is eligible for parole. [Idaho code](https://legislature.idaho.gov/statutesrules/idstat/Title18/T18CH61/SECT18-6104/) dictated that he serve anywhere from one year to life for this crime. 

Many people were overjoyed by this lengthy sentence, and to see von Ehlinger finally get his comeuppance by being labelled “criminal felon”. Many wish his sentence were longer. Some hope that he suffers badly in prison. Many viewed this sentence as “justice”. And others hoped that Priscilla Giddings be prosecuted for intimidating Jane Doe.

I also suspect his guilty verdict and his sentence are viewed as a riposte to Priscilla Giddings and her army of supporters - whose stalwart defense of von Ehlinger regardless of the facts turned out to be a very bad political calculation for them. It is also viewed as a symbolic of a criminal justice system that appears to be finally taking sexual assault seriously. After incidents like an [Idaho Sheriff insisting that most rape victime lie](https://www.oregonlive.com/pacific-northwest-news/2016/03/rape_kit_system_unnecessary_si.html), a Montana judge who [excused the sexual abuse of a 14-year-old](https://www.nbcnews.com/news/us-news/montana-judge-defends-decision-sentence-teacher-just-30-days-sex-flna8C11028610) by her teacher and the infamous [Brock Turner ‘20 minutes of action’ defense](https://www.espn.com/college-sports/story/_/id/15997412/brock-turner-father-says-son-suffer-20-minutes-action), who wouldn’t want to see rape treated like the [serious act of torture](https://ihl-databases.icrc.org/customary-ihl/eng/docs/v1_rul_rule93) that it is? 

People are *very excited* by this sentence. It feels like a major victory for ending violence against women. It’s almost exhilarating that such a large sentence was given. 

So much of this celebrating disturbs me because it is in direct conflict with the goals of reducing mass incarceration, a goal that many share. I assume readers of this blog understand that the United States’ rate of imprisonment is [off the charts](https://www.prisonpolicy.org/profiles/ID.html) compared to other nations. A common myth is that mass incarceration is largely due to non-violent drug offenses. But, prisons nationwide are filled with people who committed violent crimes - crimes that caused grievous harm to people and need redress. 

For some time, the movement to end violence against women has looked to the criminal justice system and highly punitive prison sentences to solve this, a view called [carceral feminism](https://transformharm.org/category/carceral-feminism/). And Idaho has a history of taking every punishment up a notch. Our imprisonment rate is [higher than the nationwide average](https://www.prisonpolicy.org/profiles/ID.html), as is our average sentence for rape. The U.S. average sentence length for rape is 18 years, with most serving 7 years (source: [Bureau of Justice Statistics](https://bjs.ojp.gov/content/pub/pdf/tssp18.pdf)). 

Perhaps we envision that people who commit violent crimes deserve punishment, or that they are dangerous and need to be kept away from everyone else (incapacitation), or they will be rehabilitated. But mostly, we just forget about these folks and have absolutely no idea what happens to them once they disappear into the federal or state prison systems. And what happens in U.S. prisons and jails -- allegedly for “public safety”, “justice”, “deterrence”, and “rehabilitation” -- should make us all ashamed that this is done in our names. 

## Not a Victory

Aasron von Ehlinger’s sentence is not the victory it seems like because: 

* Nothing in the sentence or Idaho prisons [ensures rehabilitation of individuals](https://www.youtube.com/watch?v=7i4o5T55jAc&feature=youtu.be), itself running contrary to the goal of public safety. The only ‘safety’ assured is the period that von Ehlinger is incarcerated.  

* We are trying to use punishment as a proxy for healing from harm. This is, at heart, a law enforcement perspective [not backed in evidence](https://allianceforsafetyandjustice.org/wp-content/uploads/documents/Crime%20Survivors%20Speak%20Report.pdf).   

* Long sentences for crimes are allegedly a “deterrence” against similar crimes. This is [likewise not backed in evidence](https://nij.ojp.gov/topics/articles/five-things-about-deterrence). The strongest deterrent is the risk that an individual will be caught for their crime, which is notoriously low for rape (hello, rape culture!). This suggests that punishing Priscilla Giddings and her troll army would have a greater impact on reducing rape than punishing von Ehlinger! (although, this is clearly not a binary choice). 

* We have still failed to hold people accountable for the harm they cause - most notably, Priscilla Giddings and her personal troll army.   

* The Idaho Legislature continues to [underfund](https://insession.idaho.gov/IIS/2022/Joint%20Finance-Appropriations%20Committee/220201_jfac_0800AM-Meeting.mp4) mental health services for victims of sexual assault (including children! children are sexually assaulted and the state lacks resources to help them).  

* the sex offender registry list actually [increases](https://www.acluidaho.org/en/press-releases/report-idahos-sex-offender-registry-counterproductive-may-harm-public-safety) the number of sex offenses committed. 

### What victories did occur: 

* Swift action by the Idaho Legislature in holding von Ehlinger accountable and forcing his resignation. The House Ethics Committee opted to listen and consider Jane Doe’s testimony, and finding it credible, they took appropriate action in a timely manner.  

* The House Ethics hearing regarding Priscilla Giddings' actions (although the outcome from that was inadequate).    

* The [revocation of Emri Moore’s Idaho legislature press credentials](https://www.idahopress.com/eyeonboise/capitol-correspondents-association-revokes-reporters-press-credentials/article_833db3cc-0744-5f57-a69d-1e5b20d86b58.html) for her filming of Jane Doe at the House Ethics Committee hearing while working for CBS2.   

* von Ehlinger’s guilty verdict. The prosecutor took the case forward and a jury convicted him. In a world when victims of sexual violence are so commonly disbelieved and dismissed, seeing this form of harm taken seriously was a welcome change.

* The widespread public support for Jane Doe from many members of the public and organization such as the [Idaho 97](https://www.theidaho97.org/). 

> Annie Hightower, who has acted as Doe’s personal attorney since the ethics hearing in April 2021, said the case was a real-time view into why survivors of sexual assault don’t report what happened to them. There is ongoing trauma associated with retelling the story, and Doe faced harassment online and when she testified at the ethics hearing last year, she said. “Today, the focus should be on my client and her healing. What happened today doesn’t heal her,” Hightower said. But the prosecutors who worked on the case helped bring her a little justice, she said.

*Source:  [Kelcie Moseley-Morris/Idaho Capitol Press](https://idahocapitalsun.com/2022/04/29/former-idaho-representative-found-guilty-of-felony-rape-in-trial-involving-legislative-intern/)*

## What else could have happened? 

* Priscilla Giddings should have been expelled from the Idaho House of Representatives. Her actions were arguable as bad as von Ehlinger's and caused untold harm to Jane Doe.  

* The shunning of Jane Doe by Boise political figures should not have happened or since it did, they should have apologized for that behavior. We in Idaho know there is an incorrigible part of the Idaho GOP that won’t do this, but Governor Little? He is capable of this.  

* Fund mental health resources for free long-term counseling for victims of sexual violence (this should not require a guilty verdict or even a police report to access).

* ….(undoubtedly, more could be done. I am not the authority on all that is possible). 

Make no mistake, von Ehlinger fucked up – badly. He caused deep harm to Jane Doe and her family. Worse, he does not appear to understand and accept the damage he caused, and with that in mind, he probably has not begun the process of trying to make things right. 

> I have to come to the conclusion that your denial, demonstrated lack of empathy….I wrote down two words. Victim and hero. You see yourself as a victim and you see yourself as a hero. I don't see you as either one of those things….Your failure to address her injuries stands in stark contrast to your claim everything was ok….There were vast differences in age, size, power, between the victim. I am persuaded you manipulated those things to your advantage….You have a pattern of explaining, excusing, deflecting, blaming others for situations you find yourself in.  

-- [Judge Reardon](https://twitter.com/dugganreports/status/1565105390622162944) at von Ehlinger's sentencing

Aaron von Ehinger’s journey to becoming a compassionate human being who cares about others will be a long one, if he ever decides to take that path. 

## What should we do? 

At the very least, we don’t have to celebrate a man being thrown in prison for likely over a decade (Idaho pardons and commissions is famous for automatically denying all initial parole requests regardless of good behavior). von Ehlinger will likely [be abused and witness abuse in prison](https://www.prisonpolicy.org/blog/2020/12/02/witnessing-prison-violence/), perhaps [sexually assaulted](https://theappeal.org/cynthia-alvarado-sexual-assault-in-prisons/), routinely dehumanized by prison staff (so many sources documenting this), and may join one of the [white supremacist gangs that operate in IDOC facilities](https://www.justice.gov/usao-id/pr/two-leaders-aryan-knights-prison-gang-each-sentenced-over-17-years-rico-conspiracy). When he emerges, likely in his 50s, he will find the process of rebuilding his life extremely difficult. With a felony record, [finding a job](https://interfaithsanctuary.org/blog/ban-the-box/) will be very challenging (since the Idaho legislature has refused repeatedly to pass “[ban the box](https://www.acluidaho.org/en/legislation/2018-sb-1307-fair-chance-employment)” legislation). As a registered sex offender, securing housing will also be very difficult. Ironically, the [sex offender registry actually worsens](https://www.acluidaho.org/en/press-releases/report-idahos-sex-offender-registry-counterproductive-may-harm-public-safety) the possibly of recidivism, that is, rather than protecting people, it actually results in a higher probability of future sexual offenses. 

What should we do about rapists? The short but honest answer is…I don’t know. How to handle interpersonal harm is challenging and complicated. This is a disappointing answer, but the lack of a clear alternative does not mean we have to embrace carceral feminism. There are alternative models in existence - [transformative](https://transformharm.org/transformative-justice-a-brief-description/) and [restorative](https://www.ojp.gov/ncjrs/virtual-library/abstracts/restorative-justice-overview) justice approaches that are routinely dismissed by the uninformed as naive despite all [that these alternative models have accomplished](https://emu.edu/now/peacebuilder/2009/10/how-effective-is-restorative-justice/).  

And yet, we as a society continually look to this punitive, violent and extremely expensive “criminal justice” system that consistently produces bad outcomes: [more damaged people](https://www.prisonpolicy.org/blog/2021/05/13/mentalhealthimpacts/), [aggressive behavior](https://www.prisonpolicy.org/blog/2021/05/13/mentalhealthimpacts/), [broken families](https://nij.ojp.gov/topics/articles/hidden-consequences-impact-incarceration-dependent-children), [generational prisoners](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=3200773), and a wave of [unintended consequences](https://www.sciencedirect.com/science/article/pii/S2666560321000025). Should we really look to the prisons and police to fix all of our social problems? 
The Ada County Prosecutor, the very office that prosecuted von Ehlinger, was [recently criticized](https://www.spokesman.com/stories/2021/may/21/we-know-there-is-controversy-ada-prosecutors-offic/) for including an photo of the ‘[thin blue line flag](https://www.themarshallproject.org/2020/06/08/the-short-fraught-history-of-the-thin-blue-line-american-flag)' *in their promotional social media posts* supporting victims of crime. Do they also support victims of police violence? Or do they only support “[perfect victims](https://survivedandpunished.org/no-perfect-victims-videos/)"? This is not the only system that exists; we don't have to buy into the argument that only police and prisons keep us safe and only prosecutors care about victims of violence. 

Do we as a society that is attempting to create and maintain social structures, want to use the power of the state to inflict violence and harm on those we've decided deserve it? Do we believe in second chances for people? If so, do we have a path for those who want a second chance to return as a contributing member of our communities? At this time in Idaho, the answer embedded in our policies is "yes" to inflicting violence and "no" to second chances. 

## Postscript {#update}

William Spence at the [Lewiston Tribune](https://lmtribune.com/) recently penned what is most accurately described as a [personal essay](https://lmtribune.com/northwest/commentary-navigating-the-line-between-sin-and-forgiveness/article_1392f503-6abf-5b9e-b62f-257d5b61f266.html) regarding how he believes "we" (really, he) ought to forgive to Von Ehlinger. In Spence's own words, he did not reveal in his reporting that several Idaho GOP leaders wrote letters of support for von Ehlinger regarding sentencing because he - Spence - could imagine himself writing such letters. This is a rather shocking admission of a member of the press making a decision to suppress information because of their personal views.

I would argue that very person deserves forgiveness, but, how we arrive at is complex and should not skim over accountability for causing harm. Spence's essay fails to address this rather spectacularly. This quote is particularly naive: "So, where’s the line when it comes to sin? At what point does the sinner become untouchable?" Well, no one is untouchable and we all deserve forgiveness, but that does not mean Von Ehlinger deserves immediate public forgiveness. At the time of publishing this, von Ehlinger has done *nothing* to repair harm - not admitted his own poor choices nor sought to repair damage.  With that in mind, does von Ehlinger deserve a public exploration of forgiveness despite making zero amends, in a major news outlet serving North Idaho? I don't think so.   

It's also important to acknowledge that Spence is not addressing the thousands of people in Idaho's prisons and jails that are separated from their loved ones. He is not asking if any of those people deserve forgiveness. von Ehlinger's privilege as a former legislator enabled this level of public discourse on forgiveness. 

As long as people are wiling to publicly say that von Ehlinger's actions are alright despite his inability to take responsibility for them, they are further enabling this lack of accountability. If Spence or others want to help von Ehlinger, send him a letter while he's in IDOC facilities. He will be there for years and will need support from the outside. He needs to hear that he has made a grievous mistake that he should own up to, and that there is a path to forgiveness -- one that starts with him trying to repair the damage he has caused. 

