---
title: "8 Guidelines for Reporting on Criminal Justice Fairly in Idaho"
date: 2022-12-03
tags: ["new media", "criminal justice", "copaganda"]
description: "The news media plays a very large role in shaping public perception of crime. Here are some guidelines on this can be done humanely"
thumbnail: /images/breaking_news.jpeg
images: ["/images/breaking_news.jpeg"]
---

News media plays a large role in shaping how the public views crime, crime rates, and criminals. While the Federal Judiciary has produced a guide for how to navigate the federal court system as a reporter (see the link at the end of this document), guidelines are scarce on how to report on these issues with sensitivity and awareness of the full suite of issues.

There are a few areas where local and regional crime coverage could improve in accuracy and ethics. Below is a list of small steps Idaho and adjacent newsrooms can take that would mean a great deal to the people they write about when reporting on crime and criminal justice.

## Guidelines

1. Do not publish mug shots either in print or online. These dehumanize people and linger on for years, affecting people’s ability to find work and housing, and to rebuild their lives. Mug shots are published on county Sheriff’s websites before individuals have been found guilty of crimes. Occasionally, people escape law enforcement custody and their mug shots are published to assist in bringing them into custody, but these incidents are rare. Many news outlets, including the Associated Press, have instituted a policy of not publishing mug shots unless warranted. 

2. Use words that humanize the individuals involved. People are more than "felons", “sex offenders”, "inmates" or "victims". If the story is about sex offenders, the use of that term may be appropriate. But if a story is broader than that, collapsing a person down to their worst act(s) may be a prejudicial approach (as always, context is important). There is no set of terms that is universally accepted, but making an effort to treat story subjects as three-dimensional human beings matters. The Marshall Project did a [series of stories](https://www.themarshallproject.org/2021/04/12/the-language-project) to address this topic and provide some guidance. 

3. Statements and press releases from police, prosecutors, and other members of the judicial systems should be fact checked. Assertions from police and members of the judicial system (outside of actual court proceedings) should be treated as quotes and/or allegations, not statements of fact. Police, prosecutors, and other members of the judicial system often have an interest in particular outcomes and should not be relied upon for impartial presentations of facts. Police and prosecutors have been known to lie to the public, either directly or through omitted facts, or by presenting misleading summaries of events. We saw this most infamously during [initial police representations of the Uvalde school shooting](https://www.texastribune.org/2022/05/27/uvalde-school-shooting-police-errors/) and the [initial press release](https://www.independent.co.uk/news/world/americas/george-floyd-police-report-murder-b1834832.html) regarding George Floyd’s murder. More minor examples persist every day, such as [“fentanyl exposure” myth](https://www.cjr.org/united_states_project/fentanyl-myth-passive-exposure-police.php). 

4. When writing about police violence, avoid using the passive voice to describe who allegedly committed a crime. “There was an officer-involved shooting” or “A person was caught in the crossfire”. These statements de-emphasize the role of police in committing violence, a misleading approach that is usually not extended to members of the public. The [AP Style guideline](https://twitter.com/APStylebook/status/1582422113499381760) has excellent recommendation on this issue: 
> We recommend avoiding the vague jargon officer-involved or police-involved. Be specific about what happened. If police use the term, ask for detail. How was the officer or officers involved? Who did the shooting? If the information is not available or not provided, say so. 

5. Be aware of the balance of coverage of crimes compared with actual crime. Does a news outlet largely cover very sensationalist crime stories? Does a story about a murder include context about how often murders happen in a particular county or community? Is a report about a violent act committed by someone out on bail without mentioning that the majority of  people freed on bail [do not commit any crimes](https://www.aclu.org/news/criminal-law-reform/what-you-need-to-know-about-cash-bail-and-crime-rates) while awaiting trial? 

6. A crime beat should include regular reporting on the system that adjudicates crime. Courts, judges, prosecutors, defense attorneys, jails, prisons, sheriffs, police officers, and so on. These are complex systems that vary greatly across jurisdictions and are not infallible. For example, are there judges known for giving long sentences for particular crimes? Is one parole officer responsible for a disproportionate number of individuals having their parole revoked? Why are particular jails overcrowded, and what does that mean for the people incarcerated there? Consider how often a news outlet addresses criminal justice issues outside of particular criminal cases. 

7. Be mindful of how news organization can play a role in fueling a trial by public mob. Perhaps the worst example of this is the [Central Park Five trial](https://www.themarshallproject.org/2019/05/31/it-s-time-to-change-the-way-the-media-covers-crime), where five young men (largely minors) were wrongfully convicted after considerable media coverage on the violent savagery of the crime. Is news coverage bringing new information to the target audience or fueling existing prejudices? Please keep in mind that news coverage will shape how the general public sees court cases and often crime overall, and remember there are real people involved for whom this is not a story – it is their life. While it is normal and appropriate to provide updates on major crimes, consider how to write about these difficult issues without creating unnecessary prejudice against a person or group of people. Attending court hearings can be a good way to obtain reliable information, hear multiple points of view, and humanize the individuals involved. 

8. Do not publish police logs. These provide a biased view of local crime, and overall, disproportionately targets poor people. Most sexual assault crimes are not reported. Incidents of police violence do not make these logs, nor do civil matters such as wage theft or white collar crime like embezzlement or misuse of public funds.  

## Additional Resources

* The Sentencing Project’s [guide to reporting on criminal justice]( https://www.sentencingproject.org/publications/10-crime-coverage-dos-and-donts/). The Sentencing Project is a national Nonprofit focused on humane responses to crime.

* Alec Karakatsanis, a former public defender and co-founder of Equal Justice Under Law, writes on [media coverage of criminal justice](https://equalityalec.substack.com/) regularly. 

* The U.S. Office of Justice Programs’ [guide to reporting on crime victims](https://www.ojp.gov/ncjrs/virtual-library/abstracts/guide-journalists-who-report-crime-and-crime-victims) (and [pdf version](http://www.mediacrimevictimguide.com/journalistguide.pdf)).  

* The U.S. Courts’ [Guide to Federal Court systems](https://www.uscourts.gov/statistics-reports/reporting-criminal-cases-journalists-guide). 

* The Police Accountability Project’s [guidelines for reporting on crime and race](https://policeaccountability.org.au/commentary/reporting-crime-and-race-a-short-guide-for-journalists/) (Australia-focused, but there is relevant information for U.S. reporting).  

* Defund MPD’s (a Washington DC-based police abolitionist organization) [guide on how to identify copaganda](https://www.defundmpd.org/resources/copaganda) in news stories.

* Marshall Project [Language of Incarceration](https://www.themarshallproject.org/2021/04/12/the-language-project), a dedicated effort to examining how the language we use to described imprisoned people affects attitudes about them. 
