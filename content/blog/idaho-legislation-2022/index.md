---
title: "Juvenile Justice and the 2022 Idaho Legislative Session"
date: "2022-01-17T23:39:06-06:00"
tags:
- Idaho Legislature
- juvenile fees
- juvenile justice
description: "In 2022, there are likely to be several pending bill in the Idaho Legislature impacting mass incarceration. In particular, some major opportunities protecting youth are slated."
thumbnail: /images/Idaho-state-capital.jpg
---

**Photo: Idaho State Capital Building, Boise, Idaho** 

## What is on the Horizon for the 2022 Idaho Legislature?

Since it is only one week into the 2022 legislation session (which began January 9), there are very few proposed bills at this time. Here are several bills slated to be introduced this session:

1. Increase the threshold for grant theft from \$1000 to \$2500. Grand theft is a felony, a charge that can have far reaching effects on a person's life beyond the term of punishment. The threshold has been set at $1000 for several decades despite inflation over that time period. 

1. Ban the use of deception practices when law enforcement officers when questions juveniles. The miniseries [When They See Us](https://www.rottentomatoes.com/tv/when_they_see_us) depicts the devastating consequences wrought when police used deceptive practices to trick the "Central Park 5" - all teenagers - into signing confessions for crimes they did not commit. 

1. Mandatory recording of all interrogations involving juveniles, an effective measure to ensure that deceptive interrogation practices are not used on juveniles. 

1. Banning the use of juvenile fees in criminal justice. Fees imposed by courts, governments have long been [recognized](https://debtorsprison.jlc.org) to trap children and their families in the criminal justice system. They are effectively an "on ramp" to the adult criminal justice system that is notoriously difficult to break free from. These fee statutes, passed in the 1980s and 1990s in Idaho, can eventually mount to thousands of dollars. Research by the [Idaho Center for Fiscal Policy](https://idahofiscal.org/wp-content/uploads/2021/12/The-Role-of-Idahos-State-Juvenile-Cost-of-Care-Fees.pdf) indicates that on average, these are 8.7% and 9.4% of (American) Indian and Black household income. Although these fees greatly impact the finances of the families charged, the fees themselves contribute very little to the cost of juvenile corrections, supporting 0.3% of the annual budget of the Idaho Department of Juvenile Correction. These fees have been banned in [Oregon](https://youthrightsjustice.org/juvenile-fees/), [California](https://finesandfeesjusticecenter.org/articles/california-sb-190-juveniles/), [Colorado](http://stand.org/colorado/blog/governor-signs-bill-end-juvenile-fees) and [New Jersey](https://www.nj.gov/governor/news/news/562022/20220110e.shtml). Note that restitution charges, monies owed as a part of court's sentence (e.g. to benefit victims), will not be affected by the proposed bill. 

Please consider signing the petition organized by the Idaho Justice Coalition supporting the removal of these fees. You can find that petition [here](https://www.idahojusticeproject.org/petition-to-eliminate-juvenile-fees). 

### 2021 Idaho Legislative Report

This was the longest legislative session on record for Idaho. These are the major bills *(note that this is a not an exhaustive list of all criminal justice related proposed legislation)*: 

#### Victories from 2021

* [Senate Bill 1200](https://legislature.idaho.gov/sessioninfo/2021/legislation/S1200/) compensation increased for individuals wrongfully imprisoned.
* [House Bill 151](https://legislature.idaho.gov/sessioninfo/2021/legislation/H0151/) effectively banning charitable bail organizations (and very clearly authored by representatives from the bail industry), died in in committee. 

#### Losses in 2021

* [House Bill 196](https://legislature.idaho.gov/sessioninfo/2021/legislation/H0196/), Fair Chance Employment or "ban the box" legislation, died in committee. 
