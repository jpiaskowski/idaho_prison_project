---
date: "2020-08-15"
title: About
---

This is largely a data-driven blog written by data scientist Julia Piaskowki (me). I have not been formerly incarcerated, but like many, I have friends and family whose lives have been greatly impacted by mass incarceration. Sadly, my experience has shown me that prisons are destructive institutions that engender violence, victimize inmates and do little, if anything, to address harm or make communities safer. 

If you want to contribute your writing, please reach out! Until I build a contact tab, the best way to reach me is via [twitter](https://twitter.com/IDprisonproject). 

----------

#### Note:  

*All content on this website is copyright (through 2024) by Julia Piaskowski. If you want to use any plots or other content for your own ends, please contact me. I will probably be okay with it, but please ask me first.*