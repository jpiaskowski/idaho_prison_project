---
date: "2020-09-01"
title: Home
---

Welcome to **Idaho Prison Project**, a blog focused on understanding Idaho's incarcerated population, Idaho's tendency towards mass incarceration and different approaches for addressing crime and violence in Idaho communities. 

Idaho Prison Project advocates for alternatives to prison, policing and the excessively punitive nature of Idaho’s prison system. We envision an Idaho that:

* believes in and supports the rehabilitation of individuals that bring harm to others;
* does not resort to mass incarceration; and 
* engages in restorative and transformative justice for people and communities. 

