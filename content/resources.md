---
date: "2022-11-27"
title: Resources
---

Are you interested in learning more about prison abolition, the conditions that led to this movement, and organizations working for justice in this area? Here are a few resources!

*Note that the exact language describing each organizations is often taken directly from their website without attribution. I don't want to gum up this section with quote marks, but I figured it is best to let each organization speak for itself rather than rely on my summary.* 

### In Idaho

* **[ACLU of Idaho](https://www.acluidaho.org/)**   The premier organization advocating for humane treatment of Idaho's incarcerated population. In addition to legal work, they conduct several campaigns including *[Voices from the Inside](https://www.acluidaho.org/en/news/stories-inside)* for the currently incarcerated to share their stores and *Fair Chance Employment*, a legislative "ban the box" proposal to help formerly incarcerated gain employment.  

* **[BarNone](https://barnoneidaho.org/)** An organization devoted to helping those recently released from a correctional facility transition to life outside and regain their lives after being incarcerated. 

* **[Book of Irving](https://bookofirving82431.com)** An inside look into living in an IDOC facility and  Eagle Pass, written by IDOC resident Patrick Irving. 

* **[Idaho Justice Project](https://www.idahojusticeproject.org)** A nonpartisan organization that elevates system-involved persons, in partnership with citizens, nonprofits, justice stakeholders, and government officials, to improve Idaho’s justice system using organizing, public education, collaboration, and advocacy. 

* **[Idaho Prison Arts Collective](https://idahoprisonarts.org/)** An organization devoted to providing opportunities for residents of the Idaho prison and re-entry communities to have transformative arts experiences. 

* **[Idaho Prison Blog](https://idahoprisonblog.blogspot.com)** An inside look into living in an IDOC facility, written by IDOC resident Dale Shackelford.

* **[Jail Medicine](http://www.jailmedicine.com/)** A thoughtful, compassionate blog focused on meeting the health needs of imprisoned populations. The head writer is a doctor serving the needs of the imprisoned in Idaho. *Note: maintenance of this blog was turned over to the American College of Correctional Physicians beginning in October of 2021.* 

* **[St. Vincent de Paul Reentry Services](https://svdpid.org/reentryservices/)** A collection of programs run by St. Vincent de Paul centered around assisting in a successful transition from incarceration back into society for returning citizens.

* **[Todos O Nadie](https://aouon-id.org/)** A grassroots civil and human rights organization fighting for the rights of formerly- and currently-incarcerated people and our families.

### Media Outlets

* **[The Marshall Project](https://www.themarshallproject.org/)**  A nonpartisan, nonprofit news organization that seeks to create and sustain a sense of national urgency about the U.S. criminal justice system.

* **[Inquest](https://inquest.org/)**  Describing itself as a "Decarceral Brainstorm", this online magazine's mission is to end mass incarceration by creating a space where the voices of those doing the thinking and the work can come together to share ideas and be heard as they pursue bold solutions.

* **[The Appeal](https://theappeal.org/)**  Original journalism on how policy, politics, and the legal system impact America’s most vulnerable people. They also produced 2 very excellent podcasts, *Justice in American** and *The Appeal Podcast*. The rank-and-file reporters recently formed a new journalistic outlet, **[The Appeal Union]**(https://www.theappealunion.com/). Nevertheless, the original Appeal contain the original work of these reporters that still provides facts and insight into the U.S. carceral and criminal justice system. 

### National Organizations

* **[Prison Policy Initiative](https://www.prisonpolicy.org)** produces cutting edge research to expose the broader harm of mass criminalization. They assemble highly impactful reports and graphics utilizing hard-to-find data sets. 

* **[Critical Resistance](http://criticalresistance.org/)** seeks to build an international movement to end the Prison Industrial Complex by challenging the belief that caging and controlling people makes us safe. They have worked on this issue for a long time and offer many resources for activists and campaigns.

* **[ACLU](https://www.aclu.org/issues/criminal-law-reform)** The national office of the ACLU conducts many important campaigns aimed at criminal justice reform including bail reform, reducing mass incarceration, and public defense reform.

.

### Miscellaneous Resources

* **[Abolitionist Toolbox](https://abolitionist.tools/)** collection of lovely visualization organised around topics such "structural harm", "beyond court", "restorative justice" and more. 

* **[Alec Karakatsanis' Blog on Copaganda](https://equalityalec.substack.com/)** A deep dive in to reporting that presents a distorted view of crime and police

------------------------

This is just a small sampling of the organizations I find effective and inspiring. There are many more organizations working on these issues on the national level. If your organisation is on the list and the description is not quite accurate, please let me know. If you know of more organisations in Idaho supporting criminal justice reform, please contact me so I can add them to list. 
